package com.hisws.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hisws.hibernate.Leaves;
import com.hisws.persistence.LeavesDAO;


import flexjson.JSONSerializer;


@Path("/leaves")
public class LeavesService {
	/*
	 * This Method is Used to handle Nurses Leave requests
	 */
	@POST
	@Path("apply/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Apply(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("\n\t Apply new Leave Request \n");
		Leaves leave = new Leaves();

		try {
			String result="Pending";
			LeavesDAO dao = new LeavesDAO();
			leave.setName(ujson.get("name").toString());
			leave.setApplyDate(ujson.get("applydate").toString());
			leave.setGrade(ujson.get("grade").toString());
			leave.setLeaveCommence(ujson.get("commencedate").toString());
			leave.setLeaveType(ujson.get("leavetype").toString());
			leave.setNic(ujson.get("NIC").toString());
			leave.setNoLeaves(ujson.getInt("nodate"));
			leave.setWard(ujson.get("ward").toString());
			leave.setReason(ujson.get("reason").toString());
			leave.setResult(result);
			
			
			if (dao.insertLeave(leave) == true) {
				status = "sucessfully Applied";
			} else {
				status = "User Already Exist!!!";
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}
	@GET
	@Path("/nursesLeaveTable")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String loadNursesTable()
	{
		
		System.out.println("\tLoad the Nurses Leave Details Table\n");
		try
		{
			LeavesDAO dao =new LeavesDAO();
			List<Leaves> detaillist = dao.getNurseLeaveDetailsTable();

			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(detaillist);
			
			
		}
		catch(Exception e){
			return e.getMessage().toString();
			
		}
	}
	@POST
	@Path("/acceptLeave")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void acceptNursesLeave(JSONObject ejson) throws Exception {
		String result = "false";
		Leaves leaves=new Leaves();
		System.out.println("Accecpt The Nurses Leaves Request "+"\t\n");

		try {
			
			LeavesDAO dao = new LeavesDAO();
			
			leaves.setId(ejson.getInt("id"));
			//boolean ans=dao.deleteLogin(ejson.getInt("id"));
			
			if(dao.acceptNursesLeaveRequest(leaves)== true){
				result = "sucessfully Deleted";
			}else {
				result = "Not Deleted sucessfully";
			}
			
			
		}

		catch (JSONException e) {
			e.printStackTrace();
			//return e.getMessage();
		} 
		result = "sucessfully updated";
		System.out.println(result);
		//return result;
	}
	@POST
	@Path("/rejectLeave")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void rejectNursesLeave(JSONObject ejson) throws Exception {
		String result = "false";
		Leaves leaves=new Leaves();
		System.out.println("Reject The Nurses Leaves Request "+"\t\n");

		try {
			
			LeavesDAO dao = new LeavesDAO();
			
			leaves.setId(ejson.getInt("id"));
			//boolean ans=dao.deleteLogin(ejson.getInt("id"));
			
			if(dao.rejectNursesLeaveRequest(leaves)== true){
				result = "sucessfully Deleted";
			}else {
				result = "Not Deleted sucessfully";
			}
			
			
		}

		catch (JSONException e) {
			e.printStackTrace();
			//return e.getMessage();
		} 
		result = "sucessfully updated";
		System.out.println(result);
		//return result;
	}
	
	@GET
	@Path("/leavesView/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String loadNursesView(@PathParam("id") String id) throws Exception
	{
		int noOfOccurence=1;
		System.out.println("\tLoad the Nurses View Details:"+id+"\n");
		try
		{
			if(noOfOccurence==1){
			int leaveID=Integer.parseInt(id);
			
			LeavesDAO dao =new LeavesDAO();
			List<Leaves> detaillist = dao.getNurseViewDetails(leaveID);
			noOfOccurence=noOfOccurence+1;
			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(detaillist);
			
			}
			else
			{
				return "Error";
			}
			
		}
		catch(Exception e){
			return e.getMessage().toString();
			
		}
	}
}
