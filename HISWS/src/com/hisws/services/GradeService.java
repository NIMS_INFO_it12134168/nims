package com.hisws.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hisws.hibernate.Grade;
import com.hisws.persistence.GradeDAO;





import flexjson.JSONSerializer;

@Path("/grade")
public class GradeService {


	@POST
	@Path("insert/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Insert(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("insert new user");
		Grade grade = new Grade();

		try {
			GradeDAO dao = new GradeDAO();
			
			String offdrate =  ujson.getString("offdutyrate");
			double offdutyrate = Double.parseDouble(offdrate);
			
			String otrate = ujson.getString("overtimerate");
			double overtimerate = Double.parseDouble(otrate);
			
			String publichdayrate = ujson.getString("publicholidayrate");
			double publicholidayrate = Double.parseDouble(publichdayrate); 
			
			String bsalary = ujson.getString("basicsalary");
			double basicsalary = Double.parseDouble(bsalary);
			
			grade.setBasicsalary(basicsalary);
			grade.setGrade(ujson.get("grade").toString());
			grade.setHourslimit(ujson.getInt("hourslimit"));
			grade.setOffdutyrate(offdutyrate);
			grade.setOvertimerate(overtimerate);
			grade.setPublicholidayrate(publicholidayrate);
			
		
			if (dao.insertGrade(grade) == true) {
				status = "sucessfully registered";
			} else {
				status = "User Already Exist!!!";
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}


	@GET
	@Path("/getWards/{dname}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDrugDetailsByDName(@PathParam("dname") String dname) {
		try {
			GradeDAO da = new GradeDAO();
			List<Grade> drugDet = da.getGrades(dname);

			System.out.println("get Nurse Details");
			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(drugDet);
		} catch (Exception e) {
			return e.getMessage().toString();
		}
	}
	
	@GET
	@Path("/getGrades")
	@Produces(MediaType.APPLICATION_JSON)
	public String getGrades(){
		System.out.println("select all Wards");
		
		try {
			GradeDAO da = new GradeDAO();
			List<Grade> names = da.getGradeNames();

			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(names);
			
		} catch (Exception e) {
			// TODO: handle exception
			return e.getMessage().toString();
		}
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Update(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("insert new user");
		Grade grade = new Grade();

		try {
			GradeDAO dao = new GradeDAO();
			
			grade.setId(ujson.getInt("id"));
			
			String offdrate =  ujson.getString("offdutyrate");
			double offdutyrate = Double.parseDouble(offdrate);
			
			String otrate = ujson.getString("overtimerate");
			double overtimerate = Double.parseDouble(otrate);
			
			String publichdayrate = ujson.getString("publicholidayrate");
			double publicholidayrate = Double.parseDouble(publichdayrate); 
			
			String bsalary = ujson.getString("basicsalary");
			double basicsalary = Double.parseDouble(bsalary);
			
			grade.setBasicsalary(basicsalary);
			grade.setGrade(ujson.get("grade").toString());
			grade.setHourslimit(ujson.getInt("hourslimit"));
			grade.setOffdutyrate(offdutyrate);
			grade.setOvertimerate(overtimerate);
			grade.setPublicholidayrate(publicholidayrate);
			
		
			if (dao.updateGrade(grade) == true) {
				status = "sucessfully updated";
			} else {
				status = "Not sucessfully Updated";
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}

	
	
	
	
}
