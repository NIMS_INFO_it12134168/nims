package com.hisws.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hisws.hibernate.Overtime;

import com.hisws.persistence.OvertimeDAO;

import flexjson.JSONSerializer;


@Path("/overtime")
public class OvertimeService {

	

	@POST
	@Path("insert/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Insert(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("insert new overtime");
		Overtime overtime = new Overtime();
		try {
			
			OvertimeDAO dao = new OvertimeDAO();
			
			overtime.setName(ujson.get("name").toString());
			overtime.setNic(ujson.get("nic").toString());
			
			overtime.setWard(ujson.get("ward").toString());
			overtime.setFromduration(ujson.getInt("fromduration"));
			overtime.setToduration(ujson.getInt("toduration"));
			overtime.setHours(ujson.getInt("hours"));
			overtime.setFromtime(ujson.getInt("fromtime"));
			overtime.setTotime(ujson.getInt("totime"));
			overtime.setFromdate(ujson.getInt("fromdate"));
			overtime.setFrommonth(ujson.get("frommonth").toString());
			overtime.setFromyear(ujson.getInt("fromyear"));
			overtime.setTodate(ujson.getInt("todate"));
			overtime.setTomonth(ujson.get("tomonth").toString());
			overtime.setToyear(ujson.getInt("toyear"));
			
			//assigning to checking method
			String ward = ujson.get("ward").toString();
			String nic = ujson.get("nic").toString();
		
			int fromduration = ujson.getInt("fromduration");
			int toduration = ujson.getInt("toduration");
			int fromtime = ujson.getInt("fromtime");
			int totime = ujson.getInt("totime");
			int fromdate = ujson.getInt("fromdate");
			int todate = ujson.getInt("todate");
			String frommonth = ujson.get("frommonth").toString();
			String tomonth = ujson.get("tomonth").toString();
			int toyear = ujson.getInt("toyear");
			int fromyear = ujson.getInt("fromyear");
			int hours = ujson.getInt("hours");
			
			
			
			
			
			if(dao.getOvertimeDetails(ward,nic,fromduration,toduration,fromtime,totime,fromdate,todate,frommonth,tomonth,toyear,fromyear,hours) == 0){
						if (dao.insertOvertime(overtime) == true) {
						status = "sucessfully Inserted ";
				}
			}		
			else{
						status ="overtime already exists";
				}
				

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}

	@GET
	@Path("/getOvertimepermonth/{ward}/{year}/{month}/{nic}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDrugDetailsByDName(@PathParam("ward") String ward,
										@PathParam("year") String year,
										@PathParam("month") String month,
										@PathParam("nic") String nic) {

		try {
			OvertimeDAO da = new OvertimeDAO();
			List<Overtime> drugDet = da.getTotalOvertime(ward,year,month,nic);

			System.out.println("get Nurse Schedule Details");
			JSONSerializer serializer = new JSONSerializer();
			System.out.println(serializer.exclude("*.class").serialize(drugDet));
			return serializer.exclude("*.class").serialize(drugDet);
			
		} catch (Exception e) {
			return e.getMessage().toString();
		}

	}

	@GET
	@Path("/getOvertime")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String nurseSchedules(JSONObject jsnObj){
		
		System.out.println(jsnObj);

		try {
			String nic = jsnObj.getString("nic");
			String month = jsnObj.getString("month");
			String year = jsnObj.getString("year");
			String ward = jsnObj.getString("ward");

			OvertimeDAO overtimedao = new OvertimeDAO();
			
			System.out.println("get Nurse Schedule Details");
			
			List<Overtime> drugDet = overtimedao.getTotalOvertime(ward, year, month, nic);
			
			System.out.println("get Nurse Schedule Details");
			JSONSerializer serializer = new JSONSerializer();
			System.out.println(serializer.exclude("*.class").serialize(drugDet));
			return serializer.exclude("*.class").serialize(drugDet);
			
		} catch (Exception e) {
			return e.getMessage().toString();
		}

	}


	
	
	
}
