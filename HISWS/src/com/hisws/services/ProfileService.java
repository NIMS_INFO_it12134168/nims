package com.hisws.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hisws.hibernate.Login;
import com.hisws.hibernate.Registration;
import com.hisws.hibernate.Upload;
import com.hisws.persistence.ProfileDAO;
import com.hisws.persistence.RegistrationDAO;

import flexjson.JSONSerializer;

@Path("/profile")
public class ProfileService {

	@GET
	@Path("/getProfileImage/{nic}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String loadNursesProfileImage(@PathParam("nic") String nic)
	{
		
		System.out.println("\tLoad the Nurses Profile Image : "+nic);
		try
		{
			ProfileDAO dao=new ProfileDAO();
			List<Upload> detaillist = dao.getNurseProfileDetails(nic);

			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(detaillist);
			
			
		}
		catch(Exception e){
			return e.getMessage().toString();
			
		}
	}
	@POST
	@Path("uploadImage/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Upload(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("\n\tRegister new user \n");
		Upload upd =new Upload();
		

		try {
			ProfileDAO dao = new ProfileDAO();
			upd.setFile(ujson.get("file").toString());
			upd.setHeight(ujson.getInt("height"));
			upd.setWidth(ujson.getInt("width"));
			upd.setSize(ujson.getInt("size"));
			upd.setNic(ujson.get("NIC").toString());
			upd.setType(ujson.get("type").toString());
			upd.setDate(ujson.getInt("date"));
			
			
			if ((dao.UploadUserImage(upd) == true)) {
				status = "sucessfully registered";
			} else {
				status = "User Already Exist!!!";
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}
	@POST
	@Path("resetPassword/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String ResetPassword(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("\n\tReset User Password \n");
		Login login =new Login();
		

		try {
			ProfileDAO dao = new ProfileDAO();
			login.setPassword(ujson.get("Password").toString());
			login.setUserName(ujson.get("username").toString());
			
			
			if ((dao.resetPassWord(login) == true)) {
				status = "Sucessfully Updated";
			} else {
				status = "Updation Failed!!!";
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}
	
}
