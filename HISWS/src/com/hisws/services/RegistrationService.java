package com.hisws.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//import net.sf.cglib.core.Converter;




import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hisws.hibernate.Grade;
import com.hisws.hibernate.Login;
import com.hisws.hibernate.Registration;
import com.hisws.hibernate.Upload;
import com.hisws.hibernate.Ward;
import com.hisws.persistence.ProfileDAO;
import com.hisws.persistence.RegistrationDAO;

import flexjson.JSONSerializer;

@Path("/registration")
public class RegistrationService {
	/*
	 * This Method is used to insert new Record
	 */
	@POST
	@Path("insert/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Insert(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("\n\tRegister new user \n");
		Registration registration = new Registration();
		Login login= new Login();

		try {
			RegistrationDAO dao = new RegistrationDAO();
			Upload upd =new Upload();
			registration.setName(ujson.get("name").toString());
			registration.setAddress(ujson.get("address").toString());
			registration.setNic(ujson.get("nic").toString());
			registration.setGender(ujson.get("gender").toString());
			registration.setCategory(ujson.get("category").toString());
			registration.setGrade(ujson.get("grade").toString());
			registration.setWard(ujson.get("ward").toString());
			registration.setDob(ujson.getString("dob").toString());
			
			login.setUserName(ujson.get("nic").toString());
			login.setPassword(ujson.get("nic").toString());
			login.setCategory(ujson.get("category").toString());
			upd.setNic(ujson.get("nic").toString());
			upd.setFile("user");
			upd.setHeight(60);
			upd.setWidth(50);
			upd.setType("jpeg");
			upd.setSize(1);
			upd.setDate(123);
			ProfileDAO pdao = new ProfileDAO();
			boolean upResult =pdao.UploadUserImage(upd);
			if ((dao.insertUser(registration,login) == true)) {
				status = "sucessfully registered";
			} else {
				status = "User Already Exist!!!";
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}
	
	@GET
	@Path("/getWards")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWardsDetails(){
		System.out.println("select all Wards");
		
		try {
			RegistrationDAO da = new RegistrationDAO();
			List<Ward> wardslist = da.getNurseWards();

			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(wardslist);
			
		} catch (Exception e) {
			// TODO: handle exception
			return e.getMessage().toString();
		}
	}
	
	@GET
	@Path("/getGrades")
	@Produces(MediaType.APPLICATION_JSON)
	public String getGradesDetails(){
		System.out.println("select all Grades");
		
		try {
			RegistrationDAO da = new RegistrationDAO();
			List<Grade> gradelist = da.getNurseGrades();

			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(gradelist);
			
		} catch (Exception e) {
			// TODO: handle exception
			return e.getMessage().toString();
		}
	}
/*
 * This Method is used to update nurses personal details
 */
	@PUT
	@Path("/update")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateNurse(JSONObject ejson) throws Exception {
		String result = "false";
		Registration registration = new Registration();
		Login login = new Login();
		System.out.println("Update Nurse Personal Infromation"+ejson.get("name").toString()+"\t\n");

		try {
			
			RegistrationDAO dao = new RegistrationDAO();
			
			registration.setId(ejson.getInt("id"));
			registration.setName(ejson.get("name").toString());
			registration.setAddress(ejson.get("address").toString());
			registration.setNic(ejson.get("nic").toString());
			registration.setGender(ejson.get("gender").toString());
			registration.setCategory(ejson.get("category").toString());
			registration.setGrade(ejson.get("grade").toString());
			registration.setWard(ejson.get("ward").toString());
			registration.setDob(ejson.get("dob").toString());
			
			login.setCategory(ejson.get("category").toString());
			login.setUserName(ejson.get("nic").toString());
			
			if(dao.updateNurse(registration,login) == true){
				result = "sucessfully updated";
			}else {
				result = "Not Updated sucessfully";
			}
			
			
		}

		catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		} 
		
		System.out.println("\n\t"+result);
		return result;
	}
	
	/*
	 * This Method is Used to delete the record in database
	 */
	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteNurse(JSONObject ejson) throws Exception {
		String result = "false";
		Registration registration = new Registration();
		System.out.println("Delete The Nurses "+"\t\n");

		try {
			
			RegistrationDAO dao = new RegistrationDAO();
			
			registration.setId(ejson.getInt("id"));
			boolean ans=dao.deleteLogin(dao.getNurseUserName(ejson.getInt("id")));
			if(dao.deleteNurse(registration) == true){
				result = "sucessfully Deleted";
			}else {
				result = "Not Deleted sucessfully";
			}
			
			
		}

		catch (JSONException e) {
			e.printStackTrace();
			//return e.getMessage();
		} 
		result = "sucessfully updated";
		System.out.println(result);
		//return result;
	}
	
	/*
	 * Function used to load Registered nurses information  table
	 */
	@GET
	@Path("/nursesTable")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String loadNursesTable()
	{
		
		System.out.println("\tLoad the Nurses Details Table\n");
		try
		{
			RegistrationDAO dao =new RegistrationDAO();
			List<Registration> detaillist = dao.getNurseDetailsTable();

			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(detaillist);
			
			
		}
		catch(Exception e){
			return e.getMessage().toString();
			
		}
	}
	
	@GET
	@Path("/nursesView/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String loadNursesView(@PathParam("id") String id) throws Exception
	{
		int noOfOccurence=1;
		System.out.println("\tLoad the Nurses View Details:"+id+"\n");
		try
		{
			if(noOfOccurence==1){
			int nurseID=Integer.parseInt(id);
			
			RegistrationDAO dao =new RegistrationDAO();
			List<Registration> detaillist = dao.getNurseViewDetails(nurseID);
			noOfOccurence=noOfOccurence+1;
			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(detaillist);
			
			}
			else
			{
				return "Error";
			}
			
		}
		catch(Exception e){
			return e.getMessage().toString();
			
		}
	}
	
	@GET
	@Path("/nursesLeaveForm/{nic}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String loadNursesLeaveForm(@PathParam("nic") String nic) throws Exception
	{
		int noOfOccurence=1;
		System.out.println("\tLoad the Nurses Leave Form Details:"+nic+"\n");
		try
		{
			if(noOfOccurence==1){
			//int nurseID=Integer.parseInt(id);
			
			RegistrationDAO dao =new RegistrationDAO();
			List<Registration> detaillist = dao.getNurseLeaveFormDetails(nic);
			noOfOccurence=noOfOccurence+1;
			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(detaillist);
			
			}
			else
			{
				return "Error";
			}
			
		}
		catch(Exception e){
			return e.getMessage().toString();
			
		}
	}
	
	
	
	@GET
	@Path("/getNursesByWard/{dname}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getNurseDetailsByWard(@PathParam("dname") String dname) {
		try {
			RegistrationDAO da = new RegistrationDAO();
			List<Registration> drugDet = da.getNurseDetailsByWard(dname);

			System.out.println("get Nurses By Ward");
			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(drugDet);
		} catch (Exception e) {
			return e.getMessage().toString();
		}
	}
	
	@GET
	@Path("/getnursename/{nic}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String GetNursesNameById(@PathParam("nic") String nic) throws Exception
	{
		int noOfOccurence=1;
		System.out.println("\tLoad the Nurses View Details:"+nic+"\n");
		try
		{
			if(noOfOccurence==1){
			
			
			RegistrationDAO dao =new RegistrationDAO();
			List<Registration> detaillist = dao.getNurseDetailsByNIC(nic);
			noOfOccurence=noOfOccurence+1;
			JSONSerializer serializer = new JSONSerializer();
			return serializer.exclude("*.class").serialize(detaillist);
			
			}
			else
			{
				return "Error";
			}
			
		}
		catch(Exception e){
			return e.getMessage().toString();
			
		}
	}
	
	
	
	
}
