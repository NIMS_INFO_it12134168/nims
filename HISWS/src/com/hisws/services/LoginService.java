package com.hisws.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hisws.hibernate.Login;
import com.hisws.persistence.LoginDAO;


@Path("/login")
public class LoginService {
	
	@POST
	@Path("checkLogin/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Insert(JSONObject ujson) throws Exception {
		String status=null;
		System.out.println("\n\t Get Nurses Login Details \n");
		LoginDAO dao =new LoginDAO();
		Login login= new Login();

		try {
			login.setPassword(ujson.getString("password"));
			login.setUserName(ujson.getString("username"));
			int ans=dao.getAuthentication(login);
			if(ans == -10)
			{
				status = "-10";
			}
			if(ans == 5)
			{
				status="5";
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}
}
