package com.hisws.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hisws.hibernate.Offdutyandpublicholiday;

import com.hisws.persistence.PublicHolidayAndOffDutyDAO;

import flexjson.JSONSerializer;

@Path("/publicholidayandoffduty")
public class PublicHolidayAndOffDutyService {

	

	@POST
	@Path("insert/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Insert(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("insert new publicholiayandofftime");
		Offdutyandpublicholiday offdutyandpublicholiday = new Offdutyandpublicholiday();

		try {
			
			
			PublicHolidayAndOffDutyDAO dao = new PublicHolidayAndOffDutyDAO();
			
			offdutyandpublicholiday.setName(ujson.get("name").toString());
			offdutyandpublicholiday.setNic(ujson.get("nic").toString());
			offdutyandpublicholiday.setGrade(ujson.get("grade").toString());
			offdutyandpublicholiday.setWard(ujson.get("ward").toString());
			offdutyandpublicholiday.setYear(ujson.getInt("year"));
			offdutyandpublicholiday.setMonth(ujson.get("month").toString());
			offdutyandpublicholiday.setDate(ujson.getInt("date"));
			offdutyandpublicholiday.setPublicholiday(ujson.getInt("publicholiday"));
			offdutyandpublicholiday.setOffday(ujson.getInt("offday"));
			
			String name = ujson.get("name").toString();
			String nic  = ujson.get("nic").toString();
			String ward = ujson.get("ward").toString();
			int year = ujson.getInt("year");
			String month = ujson.get("month").toString();
			int date = ujson.getInt("date");
			int publicholiday = ujson.getInt("publicholiday");
			int offday = ujson.getInt("offday");
			
			if(dao.getPublicHolidayAndOffDutyDetails(ward, nic, name, year, month, date, publicholiday, offday) == 0){
				if (dao.insertPublicHolidayAndOffDuty(offdutyandpublicholiday) == true) {
					status = "sucessfully Inserted";
				}
			}else{
					status = "Record Already Exist";
			}	
		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}
	
	
	@GET
	@Path("/getpublicholidayandoffdayduty")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getPublicHolidayDutyAndOffDayDuty(JSONObject jsnObj){
		
		System.out.println(jsnObj);

		try {
			String nic = jsnObj.getString("nic");
			String month = jsnObj.getString("month");
			String year = jsnObj.getString("year");
			String ward = jsnObj.getString("ward");

			PublicHolidayAndOffDutyDAO publicholidayandoffdutydao = new PublicHolidayAndOffDutyDAO();
			
			System.out.println("get Nurse Schedule Details");
			
			List<Offdutyandpublicholiday> drugDet = publicholidayandoffdutydao.getTotalOffdayAndPublicHoliday(ward, year, month, nic);			
			System.out.println("get Nurse Schedule Details");
			JSONSerializer serializer = new JSONSerializer();
			System.out.println(serializer.exclude("*.class").serialize(drugDet));
			return serializer.exclude("*.class").serialize(drugDet);
			
		} catch (Exception e) {
			return e.getMessage().toString();
		}

	}


	
	
}
