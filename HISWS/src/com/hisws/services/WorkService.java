package com.hisws.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


import com.hisws.hibernate.Work;

import com.hisws.persistence.WorkDAO;

import flexjson.JSONSerializer;

@Path("/work")
public class WorkService {

	@POST
	@Path("insert/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String Insert(JSONObject ujson) throws Exception {
		String status = "";
		System.out.println("insert new work");
		Work work = new Work();

		try {
			WorkDAO dao = new WorkDAO();

			work.setNic(ujson.get("nic").toString());
			work.setWard(ujson.get("ward").toString());
			work.setMonday(ujson.get("monday").toString());
			work.setFriday(ujson.get("friday").toString());
			work.setSaturday(ujson.get("saturday").toString());
			work.setTuesday(ujson.get("tuesday").toString());
			work.setWednesday(ujson.get("wednesday").toString());
			work.setThursday(ujson.get("thursday").toString());
			work.setFriday(ujson.get("friday").toString());
			work.setSunday(ujson.get("sunday").toString());
			work.setYear(ujson.get("year").toString());
			work.setMonth(ujson.get("month").toString());
			work.setWeek(ujson.get("week").toString());

			if (dao.checkNicMonthWeekYearAvailabiliyt(work) == 1) {
					status = "Schedule already  Exist!!!";
			} else {
				if(dao.insertWork(work)){
					status = "Scheduled sucessfully";
				}else{
					status = "Not scheduled sucessfully";
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return status;
	}

	

	@GET
	@Path("/nurseSchedules")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String nurseSchedules(JSONObject jsnObj){

		try {
			String month = jsnObj.getString("month");
			String year = jsnObj.getString("year");
			String ward = jsnObj.getString("ward");
			String week = jsnObj.getString("week");

			WorkDAO workdao = new WorkDAO();
			
			System.out.println("get Nurse Schedule Details");
			
			List<Work> drugDet = workdao.getScheduleDetailsByWardMonthYearWeek(ward, month, year, week);
			
			System.out.println("get Nurse Schedule Details");
			JSONSerializer serializer = new JSONSerializer();
			System.out.println(serializer.exclude("*.class").serialize(drugDet));
			return serializer.exclude("*.class").serialize(drugDet);
			
		} catch (Exception e) {
			return e.getMessage().toString();
		}

	}


	@GET
	@Path("/getNurseSchedule")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getnurseSchedules(JSONObject jsnObj){

		try {
			String month = jsnObj.getString("month");
			String year = jsnObj.getString("year");
			String nic = jsnObj.getString("nic");
			String week = jsnObj.getString("week");

			WorkDAO workdao = new WorkDAO();
			
			System.out.println("get Nurse Schedule Details");
			
			List<Work> drugDet = workdao.getScheduleDetailsByNicMonthYearWeek(nic, month, year, week);
			
			System.out.println("get Nurse Schedule Details");
			JSONSerializer serializer = new JSONSerializer();
			System.out.println(serializer.exclude("*.class").serialize(drugDet));
			return serializer.exclude("*.class").serialize(drugDet);
			
		} catch (Exception e) {
			return e.getMessage().toString();
		}

	}


	

	@PUT
	@Path("/update")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateSchedule(JSONObject ujson) throws Exception {
		String result = "false";
		Work work = new Work();
		System.out.println("Update Schedule Infromation");

		try {
			
			WorkDAO workdao = new WorkDAO();
			
			work.setId(ujson.getInt("id"));
			work.setMonday(ujson.get("monday").toString());
			work.setSaturday(ujson.get("saturday").toString());
			work.setTuesday(ujson.get("tuesday").toString());
			work.setWednesday(ujson.get("wednesday").toString());
			work.setThursday(ujson.get("thursday").toString());
			work.setFriday(ujson.get("friday").toString());
			work.setSunday(ujson.get("sunday").toString());
			
			if(workdao.updateSchedule(work) == true){
				result = "sucessfully updated the Schedule";
			}else {
				result = "Schedule not Updated sucessfully";
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		} 
		return result;
	}
	
	@PUT
	@Path("/delete")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String deleteSchedule(JSONObject ujson) throws Exception {
		String result = "false";
		Work work = new Work();
		System.out.println("Delete Schedule Infromation");

		try {
			
			WorkDAO workdao = new WorkDAO();
			
			work.setId(ujson.getInt("id"));
			work.setNic(ujson.get("nic").toString());
			work.setWard(ujson.get("ward").toString());
			work.setWeek(ujson.get("week").toString());
			work.setYear(ujson.get("year").toString());
			work.setMonth(ujson.get("month").toString());
			
			
			work.setMonday(ujson.get("monday").toString());
			work.setFriday(ujson.get("friday").toString());
			work.setSaturday(ujson.get("saturday").toString());
			work.setTuesday(ujson.get("tuesday").toString());
			work.setWednesday(ujson.get("wednesday").toString());
			work.setThursday(ujson.get("thursday").toString());
			work.setFriday(ujson.get("friday").toString());
			work.setSunday(ujson.get("sunday").toString());
			
			
			if(workdao.deleteSchedule(work) == true){
				result = "sucessfully deleted the Schedule";
			}else {
				result = "Schedule not Deleted sucessfully";
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
			return e.getMessage();
		} 
		return result;
	}
	
	
}
