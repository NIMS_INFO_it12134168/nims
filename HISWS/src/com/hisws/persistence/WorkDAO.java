package com.hisws.persistence;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;







import com.hisws.hibernate.Registration;
import com.hisws.hibernate.Work;

import org.hibernate.Query;

public class WorkDAO {

	Session session = null;
	Transaction tx;

	public WorkDAO() {
		// Gets the Current session
		this.session = HibernateUtil.getSessionFactory().getCurrentSession();
	}

	public Boolean insertWork(Work u) throws Exception {
		Boolean status = false;
		try {
			tx = session.beginTransaction();
			session.save(u);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			// session.close();
		}
		return status;
	}
	
	public int checkNicMonthWeekYearAvailabiliyt(Work w) throws Exception{
		
		int result = 0;
		List<Work> work = null;
		
		// int srNo = 0;
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			String nic = w.getNic();
			String week = w.getWeek();
			String month = w.getMonth();
			String year = w.getYear();
			String ward = w.getWard();
			
			String monday = w.getMonday();
			String tuesday = w.getTuesday();
			String wednesday = w.getWednesday();
			String thursday = w.getThursday();
			String friday = w.getFriday();
			String saturday = w.getSaturday();
			String sunday = w.getSunday();
	
			work = session.createQuery("FROM Work where nic ='" + nic + "' and week='"+ week +"' and month='"+ month +"' and year='"+ year +"'and ward='"+ ward +"'and monday='"+monday+"'and tuesday='"+ tuesday +"'and wednesday='"+ wednesday +"'and thursday='"+ thursday +"'and friday='"+ friday +"'and saturday='"+ saturday +"'and sunday='"+ sunday +"'").list();
			
			if(work.size() == 1){
				result = 1;
				return result;
			} 
			
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return result;
	}
	
	public List<Work> getScheduleDetailsByWardMonthYearWeek(String ward,String month,String year,String week)
			throws Exception {

		List<Work> details = null;
		// int srNo = 0;
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			details = session.createQuery("FROM Work where  ward ='" + ward + "' and  month='" + month + "' and year ='" + year + "' and week ='" + week + "'").list();

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}

	public List<Work> getScheduleDetailsByNicMonthYearWeek(String nic,String month,String year,String week)
			throws Exception {

		List<Work> details = null;
		// int srNo = 0;
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			details = session.createQuery("FROM Work where  nic ='" + nic + "' and  month='" + month + "' and year ='" + year + "' and week ='" + week + "'").list();

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
	
	public List<Work> getScheduleByNicNameWardMonthYearWeek(Work w)
			throws Exception {

		List<Work> details = null;
		// int srNo = 0;
		try {
			
			String nic  = w.getNic();
			String ward = w.getWard();
			String month = w.getMonth();
			String year = w.getYear();
			String week = w.getWeek();
			
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			details = session.createQuery("FROM Work where nic ='" + nic + "' and ward ='" + ward + "' and  month='" + month + "' and year ='" + year + "'and week ='" + week + "'").list();

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
	
	public Boolean updateSchedule(Work u) throws Exception {
		Boolean status = false;
		try {

			tx = session.beginTransaction();
			Work work = (Work) session.get(Work.class,u.getId());
			work.setMonday(u.getMonday());
			work.setTuesday(u.getTuesday());
			work.setWednesday(u.getWednesday());
			work.setThursday(u.getThursday());
			work.setFriday(u.getFriday());
			work.setSaturday(u.getSaturday());
			work.setSunday(u.getSunday());

			session.update(work);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			// session.close();
		}
		return status;
	}

	public Boolean deleteSchedule(Work u) throws Exception {
		Boolean status = false;
		try {

			tx = session.beginTransaction();
			Work work = (Work) session.get(Work.class,u.getId());
			work.setWard(u.getWard());
			work.setNic(u.getNic());
			work.setMonth(u.getMonth());
			work.setYear(u.getYear());
			work.setWeek(u.getWeek());
			work.setWard(u.getWard());
			work.setMonday(u.getMonday());
			work.setTuesday(u.getTuesday());
			work.setWednesday(u.getWednesday());
			work.setThursday(u.getThursday());
			work.setFriday(u.getFriday());
			work.setSaturday(u.getSaturday());
			work.setSunday(u.getSunday());

			session.delete(work);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return status;
	}

}
