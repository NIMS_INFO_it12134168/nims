package com.hisws.persistence;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hisws.hibernate.Grade;



public class GradeDAO {

	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	
		public Boolean insertGrade(Grade u) throws Exception {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		Boolean status = false;
		try {
			tx = session.beginTransaction();
			session.save(u);
			tx.commit();

			status = true;

			return status;
		} catch (RuntimeException ex) {
			if (tx != null && tx.isActive()) {
				try {
					tx.rollback();
				} catch (HibernateException he) {
					System.err.println("Error rolling back transaction");
				}
				throw ex;
			}
			return status;
		}
	}
	
	// Gets the Nurse Names
		public List<Grade> getGrades(String grade) throws Exception {
			
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			List<Grade> gradenames = null;
			Transaction tx = null;
			try {
				
				tx = session.beginTransaction();
				// Gets the values from the database and assign it to a List

				gradenames = session.createQuery("FROM Grade as d where grade ='" + grade + "'").list();
				// drugCat = session.createQuery("FROM MstDrugCategoryNew").list();
				
				tx.commit();
				return gradenames;
			} catch (RuntimeException ex) {
				if (tx != null && tx.isActive()) {
					try {
						tx.rollback();
					} catch (HibernateException he) {
						System.err.println("Error rolling back transaction");
					}
					throw ex;
				}
				return null;
			}
			
		}
		
		public List<Grade> getGradeNames() throws Exception {
			
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			Transaction tx = null;
			List<Grade> names = null;
			try {
				tx = session.beginTransaction();
				// Gets the values from the database and assign it to a List
					
				names = session.createQuery("FROM Grade").list();
				
				tx.commit();
				return names;

			} catch (RuntimeException ex) {
				if (tx != null && tx.isActive()) {
					try {
						tx.rollback();
					} catch (HibernateException he) {
						System.err.println("Error rolling back transaction");
					}
					throw ex;
				}
				return null;
			}
		}

		
		public Boolean updateGrade(Grade u) throws Exception {
			
			Boolean status = false;
		
			Transaction tx = null;
			
			try {
				
				tx = session.beginTransaction();
				Grade reg = (Grade) session.get(Grade.class,u.getId());
				reg.setBasicsalary(u.getBasicsalary());
				reg.setGrade(u.getGrade().toString());
				reg.setHourslimit(u.getHourslimit());
				reg.setOffdutyrate(u.getOffdutyrate());
				reg.setOvertimerate(u.getOvertimerate());
				reg.setPublicholidayrate(u.getPublicholidayrate());
				
				session.update(reg);
				tx.commit();

				status = true;
				return status;
				
			} catch (RuntimeException ex) {
				if (tx != null && tx.isActive()) {
					try {
						tx.rollback();
					} catch (HibernateException he) {
						System.err.println("Error rolling back transaction");
					}
					throw ex;
				}
				return null;
			}
		}
	
	
}
