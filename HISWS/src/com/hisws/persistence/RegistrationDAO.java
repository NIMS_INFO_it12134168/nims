package com.hisws.persistence;

import java.util.List;






import org.hibernate.CacheMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hisws.hibernate.Grade;
import com.hisws.hibernate.Login;
import com.hisws.hibernate.Registration;
import com.hisws.hibernate.Ward;
import com.hisws.persistence.HibernateUtil;


/*
 * @author IT12116652
 * P.P.A.S. Jayasundara
 * 
 * This Class is used to insert Records to the database
 * 
 */
public class RegistrationDAO {

	static Session session = null;
	static Transaction tx;

	public RegistrationDAO() {
		// Gets the Current session
		this.session = HibernateUtil.getSessionFactory().getCurrentSession();
	}
	
	public Boolean insertUser(Registration u,Login l) throws Exception {
		Boolean status = false;
		try {
			tx = session.beginTransaction();
			session.save("Registration", u);
			session.save("Login", l);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			// session.close();
		}
		return status;
	}
	@SuppressWarnings("unchecked")
	public List<Ward> getNurseWards() throws Exception {
		List<Ward> names = null;
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List

			names = session.createQuery("FROM Ward").list();
			tx.commit();
			
			

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			// session.close();
		}
		return names;
	}
	
	@SuppressWarnings("unchecked")
	public List<Grade> getNurseGrades() throws Exception {
		List<Grade> grades = null;
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List

			grades = session.createQuery("FROM Grade").list();
			tx.commit();
			

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			// session.close();
		}
		return grades;
	}
	
	public Boolean updateNurse(Registration u,Login l) throws Exception {
		Boolean status = false;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
//			Registration reg = (Registration) session.load(Registration.class,u);
			
			tx = session.beginTransaction();
			/*String sql="UPDATE Registration set Name = :newName,"+
										   "Address = :newAddress"+
										  /* "NIC = :newNIC,"+
										   "Gender = :newGender,"+
										   "Garde = :newGrade,"+
										   "Ward = :newWard,"+
										   "DOB = :newDOB"+
										   "Category = :newCategory"+*/
										//   "where id = :newID";
			/*Query query = session.createQuery(sql);*/
			//query.addEntity(Registration.class);
			/*Query query = session.createQuery("update Registration set Name = :stockName,Address = :newAddress," +
    				" where id = :stockCode");
			String name=u.getName();
			String add=u.getAddress();
			query.setParameter("stockName",name);
			query.setParameter("newAddress",add );
			/*query.setParameter("newNIC", u.getNic());
			query.setParameter("newGender",u.getGender() );
			query.setParameter("newGrade",u.getGrade() );
			query.setParameter("newWard",u.getWard() );
			query.setParameter("newDOB",u.getDob() );
			//query.setParameter("newCategory",u.getCategory() );*/
			//query.setParameter("newWard",u.getWard() );
			/*query.setParameter("stockCode",u.getId() );*/
			/*Query query = session.createQuery("update  Registration set Name = :stockName,Ward = :newWard" +
    				" where id = :stockCode");
query.setParameter("stockName", u.getName());
query.setParameter("newWard",u.getWard() );
query.setParameter("stockCode", u.getId());
int result = query.executeUpdate();*/
			
		//	System.out.println("updated result is > "+result);
		//	System.out.println("\n\tupdated result is > "+u.getGender());
			Registration reg = (Registration) session.get(Registration.class,u.getId());
			//Login log =(Login) session.get(Login.class, l.getUserName());
			//log.setCategory(l.getCategory());
			Query query = session.createQuery("update Login set Category = :LoginCat" +
    				" where UserName = :newCode");
			query.setParameter("LoginCat",u.getCategory());
			query.setParameter("newCode",u.getNic() );
			reg.setId(u.getId());
			reg.setName(u.getName());
			reg.setAddress(u.getAddress());
			reg.setCategory(u.getCategory());
			reg.setGender(u.getGender());
			reg.setGrade(u.getGrade());
			reg.setWard(u.getWard());
			reg.setNic(u.getNic());
			reg.setDob(u.getDob());
			session.saveOrUpdate(reg);
			int result = query.executeUpdate();
			System.out.println("Login Table Updated "+result);
			//session.update(log);*/
			
			
			
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			// session.close();
		}
		return status;
	}

	public Boolean deleteNurse(Registration u) throws Exception {
		Boolean status = false;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Registration reg = (Registration) session.get(Registration.class,u.getId());
			session.delete(reg);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return status;
	}
	public Boolean deleteLogin(String nic) throws Exception {
		Boolean status = false;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Query query = session.createQuery("delete Login where UserName = :userCode");
			query.setParameter("userCode", nic);
			int result = query.executeUpdate();
			session.clear();
			tx.commit();
			if(result !=0)
			{
				status = true;
			}
			

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return status;
	}
	/*
	 * Load the data nurses details Table from database
	 */
	@SuppressWarnings("unchecked")
	public List<Registration> getNurseDetailsTable() throws Exception {
		List<Registration> details = null;
		try {
			tx = session.beginTransaction();
			details = session.createQuery("FROM Registration").list();
			tx.commit();

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			// session.close();
		}
		return details;
	}
	
	@SuppressWarnings("unchecked")
	public List<Registration> getNurseLeaveFormDetails(String nic) throws Exception {

		List<Registration> details = null;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			/*Registration ci = (Registration) session.get(Registration.class, name);*/
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Registration reg = (Registration) session.load(Registration.class, 1);
			String sql = "SELECT * FROM Registration  WHERE NIC = :reg_nic";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Registration.class);
			query.setParameter("reg_nic", nic);
			details= query.list();
			session.clear();
			tx.commit();
			
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
	@SuppressWarnings("unchecked")
	public List<Registration> getNurseViewDetails(int name)
			throws Exception {

		List<Registration> details = null;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			/*Registration ci = (Registration) session.get(Registration.class, name);*/
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Registration reg = (Registration) session.load(Registration.class, name);
			String sql = "SELECT * FROM Registration  WHERE id = :reg_id";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Registration.class);
			query.setParameter("reg_id", name);
			details= query.list();
			session.clear();
			tx.commit();
			
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
	
	public String getNurseUserName(int id) throws Exception {
		String nic = null;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Registration reg = (Registration) session.get(Registration.class,id);
			String sql = "SELECT * FROM Registration  WHERE id = :reg_id";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Registration.class);
			query.setParameter("reg_id",id);
			List<Registration> ls = query.list();
	        System.out.println("query executed");
	        for (Registration u:ls) {
	        	nic=u.getNic();
	            System.out.println("UserName:" + u.getNic());
	        }
			
			tx.commit();

			
		} catch (HibernateException e) {
			
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return nic;
	}
	
	/////pubudu
	
	public List<Registration> getNurseDetailsByWard(String name)
			throws Exception {

		List<Registration> details = null;
		// int srNo = 0;
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			details = session.createQuery("FROM Registration as d where ward ='" + name + "'").list();

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
	
	@SuppressWarnings("unchecked")
	public List<Registration> getNurseDetailsByNIC(String nic )
			throws Exception {

		List<Registration> details = null;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			/*Registration ci = (Registration) session.get(Registration.class, name);*/
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Registration reg = (Registration) session.load(Registration.class, 1);
			String sql = "SELECT * FROM Registration  WHERE nic = :reg_id";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Registration.class);
			query.setParameter("reg_id", nic);
			details= query.list();
			session.clear();
			tx.commit();
			
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
	
	
	
	
	
	
}
