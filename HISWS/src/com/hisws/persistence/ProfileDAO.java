package com.hisws.persistence;

import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hisws.hibernate.Leaves;
import com.hisws.hibernate.Login;
import com.hisws.hibernate.Registration;
import com.hisws.hibernate.Upload;

public class ProfileDAO {
	static Session session = null;
	static Transaction tx;

	public ProfileDAO() {
		// Gets the Current session
		this.session = HibernateUtil.getSessionFactory().getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Upload> getNurseProfileDetails(String nic) throws Exception {

		List<Upload> details = null;
		
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Upload lev= (Upload) session.load(Upload.class, 1);
			String sql = "SELECT * FROM Upload  WHERE nic = :reg_id order by id desc limit 1";
			
			SQLQuery query = session.createSQLQuery(sql);
			//query.addEntity(Upload.class);
			query.setParameter("reg_id", nic);
			details= query.list();
			session.clear();
			tx.commit();
			
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
	public Boolean UploadUserImage(Upload u) throws Exception {
		Boolean status = false;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			session.save("Upload", u);
		
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			// session.close();
		}
		return status;
	}
	public boolean resetPassWord(Login u) throws Exception {

		boolean status=false;
		
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			@SuppressWarnings("unused")
			Login log= (Login) session.load(Login.class, 1);
			Query query = session.createQuery("update Login set Password = :Loginpw" +
    				" where UserName = :Loginun");
			query.setParameter("Loginpw",u.getPassword());
			query.setParameter("Loginun",u.getUserName());
			int result = query.executeUpdate();
			if(result != 0)
			{
				status=true;
			}
			else
			{
				status=false;
			}
			session.clear();
			tx.commit();
			
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return status;
	}
}
