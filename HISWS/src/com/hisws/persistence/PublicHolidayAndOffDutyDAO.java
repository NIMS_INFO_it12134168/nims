package com.hisws.persistence;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hisws.hibernate.Offdutyandpublicholiday;
import com.hisws.hibernate.Overtime;


public class PublicHolidayAndOffDutyDAO {
	Session session = HibernateUtil.getSessionFactory().getCurrentSession();


	public Boolean insertPublicHolidayAndOffDuty(Offdutyandpublicholiday u) throws Exception {
		
		Transaction tx = null;
		Boolean status = false;
		try {
			tx = session.beginTransaction();
			session.save(u);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			//session.close();
		}
		return status;
	}
	
	public int getPublicHolidayAndOffDutyDetails(String ward,String nic,String name,int year,String month,int date,int publicholiday,int offday)
			throws Exception {
		int result = 0;
		// int srNo = 0;
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			Query query  = session.createQuery("FROM Offdutyandpublicholiday as d where ward = :ward and nic = :nic and name = :name and month = :month and date = :date and year = :year and publicholiday = :publicholiday and offday = :offday");
			query.setParameter("ward", ward);
			query.setParameter("nic", nic);
			query.setParameter("name", name);
			query.setParameter("year", year);
			query.setParameter("month", month);
			query.setParameter("date", date);
			query.setParameter("publicholiday", publicholiday);
			query.setParameter("offday", offday);
			
			
			result =  query.list().size();
			if(result == 1){
				return result = 1;
			}else{
				return result = 0;
			}
			
					
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return result;
	}
	
	
	public List<Offdutyandpublicholiday> getTotalOffdayAndPublicHoliday(String ward,String year,String month,String nic)
			throws Exception {

	
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			
			List<Offdutyandpublicholiday> overtime  = session.createQuery("FROM Offdutyandpublicholiday as d where ward ='" + ward + "' and year ='" + year + "' and month ='" + month + "' and nic ='" +nic+ "'").list();
			tx.commit();
			return overtime;
			
		} catch (RuntimeException ex) {
			if (tx != null && tx.isActive()) {
				try {
					tx.rollback();
				} catch (HibernateException he) {
					System.err.println("Error rolling back transaction");
				}
				throw ex;
			}
			return null;
		}
	
		
	}
	
}
