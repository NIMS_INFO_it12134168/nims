package com.hisws.persistence;

import java.util.Calendar;
import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hisws.hibernate.Leaves;
//import com.hisws.hibernate.Registration;
import com.hisws.hibernate.Registration;

public class LeavesDAO {
	static Session session = null;
	static Transaction tx;

	public LeavesDAO() {
		// Gets the Current session
		this.session = HibernateUtil.getSessionFactory().getCurrentSession();
	}
	
	public Boolean insertLeave(Leaves u) throws Exception {
		Boolean status = false;
		try {
			tx = session.beginTransaction();
			session.save(u);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			// session.close();
		}
		return status;
	}
	public List<Leaves> getNurseLeaveDetailsTable() throws Exception {
		List<Leaves> details = null;
		Calendar c= Calendar.getInstance();
		int year =c.get(Calendar.YEAR);
		int month=c.get(Calendar.MONTH)+1;
		String day=("0"+month+"-"+year).toString();
	
		try {
			tx = session.beginTransaction();
			String sql = "SELECT * FROM Leaves  WHERE ApplyDate like '" +"%"+day+"'";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Leaves.class);
			//query.setParameter("reg_id", day);
			details= query.list();
			//details = session.createQuery("FROM Leaves").list();
			tx.commit();

		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			// session.close();
		}
		return details;
	}
	public Boolean acceptNursesLeaveRequest(Leaves u) throws Exception {
		Boolean status = false;
		try {

			tx = session.beginTransaction();
			
			Query query = session.createQuery("update Leaves set Result = :acceptResult" +
    				" where id = :leaveID");
			query.setParameter("acceptResult","Accepted");
			query.setParameter("leaveID",u.getId());
			int result = query.executeUpdate();
			tx.commit();
			if(result != 0){
			status = true;
			}
			else
			{
				status=false;
			}

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return status;
	}
	public Boolean rejectNursesLeaveRequest(Leaves u) throws Exception {
		Boolean status = false;
		try {

			tx = session.beginTransaction();
			
			Query query = session.createQuery("update Leaves set Result = :acceptResult" +
    				" where id = :leaveID");
			query.setParameter("acceptResult","Rejected");
			query.setParameter("leaveID",u.getId());
			int result = query.executeUpdate();
			tx.commit();
			if(result != 0){
			status = true;
			}
			else
			{
				status=false;
			}

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return status;
	}
	public List<Leaves> getNurseViewDetails(int leaveID)
			throws Exception {

		List<Leaves> details = null;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			Leaves lev= (Leaves) session.load(Leaves.class, leaveID);
			String sql = "SELECT * FROM Leaves  WHERE id = :reg_id";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Leaves.class);
			query.setParameter("reg_id", leaveID);
			details= query.list();
			session.clear();
			tx.commit();
			
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return details;
	}
}
