package com.hisws.persistence;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


import com.hisws.hibernate.Overtime;


import org.hibernate.Query;


public class OvertimeDAO {


	Session session = HibernateUtil.getSessionFactory().getCurrentSession();


	public Boolean insertOvertime(Overtime u) throws Exception {
		
		Transaction tx = null;
		Boolean status = false;
		try {
			tx = session.beginTransaction();
			session.save(u);
			tx.commit();

			status = true;

		} catch (HibernateException e) {
			status = false;
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return status;
	}
	
	public int getOvertimeDetails(String ward,String nic,int fromduration,int toduration,int fromtime,int totime,int fromdate,int todate,String frommonth,String tomonth,int toyear,int fromyear,int hours)
			throws Exception {
		int result = 0;
		// int srNo = 0;
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			Query query  = session.createQuery("FROM Overtime as d where ward = :ward and nic = :nic and fromduration = :fromduration and toduration = :toduration and fromtime = :fromtime and totime = :totime and fromdate = :fromdate and todate = :todate and frommonth = :frommonth and tomonth = :tomonth and toyear = :toyear and fromyear = :fromyear and hours = :hours");
			query.setParameter("ward", ward);
			query.setParameter("nic", nic);
			query.setParameter("fromduration",fromduration);
			query.setParameter("toduration", toduration);
			query.setParameter("fromtime", fromtime);
			query.setParameter("totime", totime);
			query.setParameter("fromdate", fromdate);
			query.setParameter("todate", todate);
			query.setParameter("frommonth",frommonth );
			query.setParameter("tomonth",tomonth );
			query.setParameter("toyear", toyear);
			query.setParameter("fromyear", fromyear);
			query.setParameter("hours", hours);
			
			
			result =  query.list().size();
			if(result == 1){
				return result = 1;
			}else{
				return result = 0;
			}
			
					
		} catch (HibernateException e1) {
			if (tx != null) {
				tx.rollback();
				e1.printStackTrace();
			}
		} finally {
			//session.close();
		}
		return result;
	}
	

	public List<Overtime> getTotalOvertime(String ward,String year,String month,String nic)
			throws Exception {

	
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			// Gets the values from the database and assign it to a List
			
			List<Overtime> overtime  = session.createQuery("FROM Overtime as d where ward ='" + ward + "' and fromyear ='" + year + "' and frommonth ='" + month + "' and nic ='" +nic+ "'").list();
			tx.commit();
			return overtime;
			
		} catch (RuntimeException ex) {
			if (tx != null && tx.isActive()) {
				try {
					tx.rollback();
				} catch (HibernateException he) {
					System.err.println("Error rolling back transaction");
				}
				throw ex;
			}
			return null;
		}
	
		
	}
	
	

}
