package com.hisws.persistence;

import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hisws.hibernate.Login;



public class LoginDAO {
	
	static Session session = null;
	static Transaction tx;

	@SuppressWarnings("static-access")
	public LoginDAO() {
		// Gets the Current session
		this.session = HibernateUtil.getSessionFactory().getCurrentSession();
	}
	public int getAuthentication(Login l) throws Exception {
		int result=0;
		
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.setCacheMode(CacheMode.IGNORE);
			tx = session.beginTransaction();
			@SuppressWarnings("unused")
			Login log = (Login) session.get(Login.class,1);
			String sql = "SELECT * FROM Login  WHERE UserName = :uname and PassWord = :pword";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Login.class);
			query.setParameter("uname",l.getUserName());
			query.setParameter("pword",l.getPassword());
			@SuppressWarnings("unchecked")
			List<Login> ls = query.list();
			if(ls.isEmpty()==true)
			{
				System.out.println("Authentication Failed");
				result=-10;
			}
			else {
				System.out.println("Authentication Passed");
				String cat =null;
				String mcat="M";
				for (Login lg:ls) {
		        	cat=lg.getCategory();
		        }
				if(cat.contentEquals(mcat))
				{	System.out.println("Category "+cat);
					result=5;
				}
				else
				{	System.out.println("Category "+cat);
					result=-5;
				}
			}
			tx.commit();

			
		} catch (HibernateException e) {
			
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (Exception ex) {
		} finally {
			 //session.close();
		}
		return result;
	}

}
