package com.hisws.hibernate;

// Generated Aug 08, 2014 12:01:27 PM by Hibernate Tools 3.4.0.CR1

/**
 * Ward generated by hbm2java
 */
public class Ward implements java.io.Serializable {

	private Integer id;
	private String ward;

	public Ward() {
	}

	public Ward(String ward) {
		this.ward = ward;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWard() {
		return this.ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

}
