$('#name, #name_label').hide();
$('#ward').change(function(){
    var state_id = $('#ward').val();
    if (state_id != ""){
        var post_url = "/index.php/person/get_names/" + ward;
        $.ajax({
            type: "POST",
             url: post_url,
             success: function(names) //we're calling the response json array 'cities'
              {
                $('#f_city').empty();
                $('#f_city, #f_city_label').show();
                   $.each(names,function(id,name) 
                   {
                    var opt = $('<option />'); // here we're creating a new select option for each group
                      opt.val(id);
                      opt.text(name);
                      $('#name').append(opt); 
                });
               } //end success
         }); //end AJAX
    } else {
        $('#ward').empty();
        $('#name, #name_label').hide();
    }//end if
}); //end change



function calc(){
   var textValue1 = document.getElementById('hours').value;
   var textValue2 = document.getElementById('overtimerate').value;
 
    if($.trim(textValue1) != '' && $.trim(textValue2) != ''){
   document.getElementById('amount').value = textValue1 * textValue2; 
    }
}

$(function(){
    $('#hours').blur(calc);
    $('#overtimerate').blur(calc);
}); 

