/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function getWards()
{
    document.getElementById('ward').options.length = 0;
    
    $.ajax({
        url: 'http://localhost/health/index.php/work/getWards',
        type: 'POST',
        crossDomain: true,
        success: function(data) {
            var json_parsed = $.parseJSON(data);
            for (var i = 0; i < json_parsed.length; i++) {
                var newOption = $('<option>');
                newOption.attr('value', json_parsed[i]['ward']).text(json_parsed[i]['ward']);
                $('#ward').append(newOption);
            }
        }
    });
}




function getNursesByWard()
{
    
    var val;
    val = $("#ward option:selected").text();
    
    document.getElementById('name').options.length = 0;
    
    $.ajax({
        url: 'http://localhost/health/index.php/work/getNurseDetailsByWard/' + val,
        type: 'POST',
        crossDomain: true,
        success: function(data) {
             var json_parsed = $.parseJSON(data);
            for (var i = 0; i < json_parsed.length; i++) {
                var newOption = $('<option>');
                var name = json_parsed[i]['name'];
                var nic = json_parsed[i]['nic'];
                var namenic = name+"-"+nic;
                newOption.attr('value',namenic).text(namenic);
                $('#name').append(newOption);
            }
        }
    });
}



function addScheduleToController()
{
     var ward = $("#ward option:selected").text();
     var namenic = $("#name option:selected").text();
     var splitNameNic = namenic.split("-");
     var name = splitNameNic[0];
     var nic = splitNameNic[1];
   
    var week = $("#week option:selected").text();
     
     var monday = $("#monday option:selected").text();
     var tuesday = $("#tuesday option:selected").text();
     var wednesday = $("#wednesday option:selected").text();
     var thursday = $("#thursday option:selected").text();
     var friday = $("#friday option:selected").text();
     var saturday = $("#saturday option:selected").text();
     var sunday = $("#sunday option:selected").text();
     
     var YearandMonth = $("#month").val();
     var splitYearandMonth = YearandMonth.split("-");
     
     var year = splitYearandMonth[0];
     var month = splitYearandMonth[1];
     
     
     if (week.length == 0 || YearandMonth.length == 0 || namenic.length == 0 || monday.length == 0 || tuesday.length == 0|| wednesday.length == 0|| thursday.length == 0 || friday.length == 0|| saturday.length == 0|| sunday.length == 0)
    {
        alert('Please Fill all the Fields in the Schedule');
    }
    else
    {
        $.ajax({
            url: 'http://localhost/health/index.php/work/addSchedule',
            type: 'POST',
            crossDomain: true,
            data: {"name": name, 
                   "nic": nic,
                   "week":week,
                   "ward":ward,
                   "year":year,
                   "month":month,
                   "monday":monday,
                   "tuesday": tuesday,
                   "wednesday": wednesday, 
                   "thursday": thursday,
                   "friday":friday,
                   "saturday":saturday,
                   "sunday":sunday
            },
            success: function(data) {
                alert(data);
                 $("#name").val("");
                 $("#ward").val("");
                 $("#nic").val("");
                 $("#week").val("");
                 $("#month").val("");
            
                 $("#monday option:selected").empty();
                 $("#tuesday option:selected").empty();
                 $("#wednesday option:selected").empty();
                 $("#thursday option:selected").empty();
                 $("#friday option:selected").empty();
                 $("#saturday option:selected").empty();
                 $("#sunday option:selected").empty();
            }
        });
    }
}

function updateScheduleToController()
{
     
     var id = $("#id").val();
     var monday = $("#monday option:selected").text();
     var tuesday = $("#tuesday option:selected").text();
     var wednesday = $("#wednesday option:selected").text();
     var thursday = $("#thursday option:selected").text();
     var friday = $("#friday option:selected").text();
     var saturday = $("#saturday option:selected").text();
     var sunday = $("#sunday option:selected").text();
     
    if (id.length == 0 || monday.length == 0 || tuesday.length == 0)
    {
        alert('Please Fill all the Fields in the Schedule to update');
    }
    else
    {
        $.ajax({
            url: 'http://127.0.0.1/NIMSINFO/index.php/work/updateSchedule',
            type: 'POST',
            crossDomain: true,
            data: {"id":id,
                   "monday":monday,
                   "tuesday": tuesday,
                   "wednesday": wednesday, 
                   "thursday": thursday,
                   "friday":friday,
                   "saturday":saturday,
                   "sunday":sunday
            },
            success: function(data) {
                alert(data);
                 $("#name").val("");
                 $("#address").val("");
                 $("#nic").val("");
            }
        });
    } 
}

function deleteScheduleToController()
{
     var ward = $("#ward option:selected").text();
     var namenic = $("#name option:selected").text();
     var splitNameNic = namenic.split("-");
     var name = splitNameNic[0];
     var nic = splitNameNic[1];
   
    var week = $("#week option:selected").text();
    var id = $("#id option:selected").text();
    
     
     var monday = $("#monday option:selected").text();
     var tuesday = $("#tuesday option:selected").text();
     var wednesday = $("#wednesday option:selected").text();
     var thursday = $("#thursday option:selected").text();
     var friday = $("#friday option:selected").text();
     var saturday = $("#saturday option:selected").text();
     var sunday = $("#sunday option:selected").text();
     
     var YearandMonth = $("#month").val();
     var splitYearandMonth = YearandMonth.split("-");
     
     var year = splitYearandMonth[0];
     var month = splitYearandMonth[1];
     
     
      if (name.length == 0 || year.length == 0 || nic.length == 0)
    {
        alert('Please Fill all the Fields in the Schedule to update');
    }
    else
    {
        $.ajax({
            url: 'http://localhost/health/index.php/work/deleteSchedule',
            type: 'POST',
            crossDomain: true,
            data: {"name": name, 
                   "nic": nic,
                   "week":week,
                   "id":id,
                   "ward":ward,
                   "year":year,
                   "month":month,
                   "monday":monday,
                   "tuesday": tuesday,
                   "wednesday": wednesday, 
                   "thursday": thursday,
                   "friday":friday,
                   "saturday":saturday,
                   "sunday":sunday
            },
            success: function(data) {
                alert(data);
                    
                 $("#name").val("");
                 $("#ward").val("");
                 $("#nic").val("");
                 $("#week").val("");
                 $("#month").val("");
                 
                 $("#monday option:selected").empty();
                 $("#tuesday option:selected").empty();
                 $("#wednesday option:selected").empty();
                 $("#thursday option:selected").empty();
                 $("#friday option:selected").empty();
                 $("#saturday option:selected").empty();
                 $("#sunday option:selected").empty();
            }
        });
    } 
}

function getScheduleFromController(){
    
     var ward = $("#ward option:selected").text();
     var week = $("#week option:selected").text();
     var YearandMonth = $("#month").val();
     var splitYearandMonth = YearandMonth.split("-");
     
     var year = splitYearandMonth[0];
     var month = splitYearandMonth[1];
     
     
    if (ward.length == 0 || year.length == 0 || week.length == 0 || month == 0)
    {
        alert('Please Fill all the Fields in the Schedule');
    }
    else
    {
        $.ajax({
            url: 'http://127.0.0.1/NIMSINFO/index.php/work/getNursesSchedule',
            type: 'POST',
            crossDomain: true,
            data: {"week":week,
                   "ward":ward,
                   "year":year,
                   "month":month},
            success: function(data) {
             var json_parsed = $.parseJSON(data);
             createScheduleViewTblOC(json_parsed);
        }
    });              
    }
}

function createScheduleViewTblOC(json){
   
    
    var pagecontent = "";

    pagecontent = "<table class=table1 id=weektable>";
    pagecontent += "<thead>";
    pagecontent += "<tr>";
    pagecontent += "<th>ID</th>";
    pagecontent += "<th>NIC</th>";
    pagecontent += "<th>Month</th>";
    pagecontent += "<th>Week</th>";
    pagecontent += "<th>Monday</th>";
    pagecontent += "<th>Tuesday</th>";
    pagecontent += "<th>Wednesday</th>";
    pagecontent += "<th>Thursday</th>";
    pagecontent += "<th>Friday</th>";
    pagecontent += "<th>Saturday</th>";
    pagecontent += "<th>Sunday</th>";
    pagecontent += "<th>Action</th>";
    pagecontent += "</tr>";
    pagecontent += "</thead>";
    pagecontent += "<tbody>";
    
    var i=1;
    $.each(json, function(index,el) {
            // el = object in array
            // access attributes: el.Id, el.Name, etc
            //alert(el.drugs.dName);
            
    if(el.processed==null || el.processed==false)
    {
    
    pagecontent += "<tr>";
    pagecontent += "<td name=reqid"+i+" id=reqid"+i+">"+el.id+"</td>";
    //pagecontent += "<td name=sr_val"+i+" id=sr_val"+i+" >"+el.name+"</td>";
    pagecontent += "<td name=req_dname"+i+" id=req_dname"+i+" >"+el.nic+"</td>";
    pagecontent += "<td><input size=6 type=text name=month"+el.id+" id=month"+el.id+" value="+el.month+"-"+el.year+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=week"+el.id+" id=week"+el.id+" value="+el.week+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=monday"+el.id+" id=monday"+el.id+" value="+el.monday+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=tuesday"+el.id+" id=tuesday"+el.id+" value="+el.tuesday+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=wednesday"+el.id+" id=wednesday"+el.id+" value="+el.wednesday+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=thursday"+el.id+" id=thursday"+el.id+" value="+el.thursday+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=friday"+el.id+" id=friday"+el.id+" value="+el.friday+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=saturday"+el.id+" id=saturday"+el.id+" value="+el.saturday+" disabled></input></td>";
    pagecontent += "<td><input size=6 type=text name=sunday"+el.id+" id=sunday"+el.id+" value="+el.sunday+" disabled></input></td>";
  
    
    pagecontent += "<td><input type=submit value=Update onclick=getAllRequestBC('"+el.id+"','"+el.nic+"')></td>";
    i++;    
    }
    pagecontent += "</tr>";
      
    });

    pagecontent += "</tbody>";
    pagecontent += "</table>";
    

   document.getElementById("pagespaceTC").innerHTML = pagecontent;
   
}

function getAllRequestBC(id,nic)
{
   
    document.getElementById('update').scrollIntoView();
    
    var week                 = $("#weektable #week"+id).val();
    var month                = $("#weektable #month"+id).val();
    var monday               = $("#weektable #monday"+id).val();
    var tuesday              = $("#weektable #tuesday"+id).val();
    var wednesday            = $("#weektable #wednesday"+id).val();
    var thursday             = $("#weektable #thursday"+id).val();
    var friday               = $("#weektable #friday"+id).val();
    var saturday             = $("#weektable #saturday"+id).val();
    var sunday               = $("#weektable #sunday"+id).val();
    
    alert(id+""+nic+""+month+""+week+""+monday+""+tuesday+""+wednesday+""+thursday+""+friday+""+saturday+""+sunday+""+month);
    document.getElementById("id").value = id;
    //document.getElementById("name").value = name;
    document.getElementById("nic").value = nic;
    
   
    $("#weekupdate").val(week);
    $("#monthupdate").val(month);
    $("#monday").val(monday);
    $("#tuesday").val(tuesday);
    $("#wednesday").val(wednesday);
    $("#thursday").val(thursday);
    $("#friday").val(friday);
    $("#saturday").val(saturday);
    $("#sunday").val(sunday);
}



window.onload = loadtabledata(nic);

function loadtabledata(nic){
    
    //$("#monday"+nic).options.length = 0;
    
    $.ajax({
        url: 'http://localhost/health/index.php/work/getSchedules',
        type: 'POST',
        crossDomain: true,
        success: function(data) {
            var json_parsed = $.parseJSON(data);
            for (var i = 0; i < json_parsed.length; i++) {
                var newOption = $('<option>');
                newOption.attr('value', json_parsed[i]['name']).text(json_parsed[i]['name']);                
                $('#monday'+nic).append(newOption);
            }
        }
    });
}



function getNursesInfoByWard()
{
    var ward = $("#ward").val();
   
    $.ajax({
        url: 'http://127.0.0.1/NIMSINFO/index.php/work/getNurseDetailsByWard/' + ward,
        type: 'POST',
        crossDomain: true,
        success: function(data) {
            var json_parsed = $.parseJSON(data);
            createScheduleViewTblRC(json_parsed,ward);
        }
    });
}



function createScheduleViewTblRC(json,ward){

    var wardName = ward;
    
    var pagecontent = "";

    pagecontent = "<table class=table1 id=pagetable>";
    pagecontent += "<thead>";
    pagecontent += "<tr>";
    pagecontent += "<th>ID</th>";
    pagecontent += "<th>Name</th>";
    pagecontent += "<th>NIC</th>";
    pagecontent += "<th>Month</th>";
    pagecontent += "<th>Week</th>";
    pagecontent += "<th>Monday</th>";
    pagecontent += "<th>Tuesday</th>";
    pagecontent += "<th>Wednesday</th>";
    pagecontent += "<th>Thursday</th>";
    pagecontent += "<th>Friday</th>";
    pagecontent += "<th>Saturday</th>";
    pagecontent += "<th>Sunday</th>";
    pagecontent += "<th>Action</th>";
    pagecontent += "</tr>";
    pagecontent += "</thead>";
    pagecontent += "<tbody>";
    
    var i=1;
    $.each(json, function(index,el) {
            // el = object in array
            // access attributes: el.Id, el.Name, etc
            //alert(el.drugs.dName);
            
    if(el.processed==null || el.processed==false)
    {
    var night = "night";
    var evening ="evening";
    var day = "day";
    var WEEKI = "WEEKI";
    var WEEKII = "WEEKII";
    var WEEKIII = "WEEKIII";
    var WEEKIV = "WEEKIV";
    var WEEKV = "WEEKV";
    var select ="select";
    
    pagecontent += "<tr>";
    pagecontent += "<td name=reqid"+i+" id=reqid"+i+" size = 5>"+el.id+"</td>";
    pagecontent += "<td name=name"+el.nic+" id=name"+el.nic+" >"+el.name+"</td>";
    pagecontent += "<td name=nic"+el.nic+" id=nic"+el.nic+" >"+el.nic+"</td>";
    pagecontent += "<td ><input size = 2 type=month name=month"+el.nic+" id=month"+el.nic+"></input></td>";
    pagecontent += "<td><select  name=week"+el.nic+" id=week"+el.nic+"><option>"+select+"</option><option>"+WEEKI+"</option><option>"+WEEKII+"</option><option>"+WEEKIII+"</option><option>"+WEEKIV+"</option><option>"+WEEKV+"</option></select></td>";
    
    pagecontent += "<td><select  name=monday"+el.nic+" id=monday"+el.nic+"><option>"+select+"</option><option>"+night+"</option><option>"+evening+"</option><option>"+day+"</option></select></td>";
    pagecontent += "<td><select  name=tuesday"+el.nic+" id=tuesday"+el.nic+"><option>"+select+"</option><option>"+night+"</option><option>"+evening+"</option><option>"+day+"</option></select></td>";
    pagecontent += "<td><select  name=wednesday"+el.nic+" id=wednesday"+el.nic+"><option>"+select+"</option><option>"+night+"</option><option>"+evening+"</option><option>"+day+"</option></select></td>";
    pagecontent += "<td><select  name=thursday"+el.nic+" id=thursday"+el.nic+"><option>"+select+"</option><option>"+night+"</option><option>"+evening+"</option><option>"+day+"</option></select></td>";
    pagecontent += "<td><select  name=friday"+el.nic+" id=friday"+el.nic+"><option>"+select+"</option><option>"+night+"</option><option>"+evening+"</option><option>"+day+"</option></select></td>";
    pagecontent += "<td><select  name=saturday"+el.nic+" id=saturday"+el.nic+"><option>"+select+"</option><option>"+night+"</option><option>"+evening+"</option><option>"+day+"</option></select></td>";
    pagecontent += "<td><select  name=sunday"+el.nic+" id=sunday"+el.nic+"><option>"+select+"</option><option>"+night+"</option><option>"+evening+"</option><option>"+day+"</option></select></td>";
  
    
    pagecontent += "<td><input type=submit value=Approve onclick=getAllRequestRC('"+el.nic+"','"+wardName+"')></td>";
    i++;    
    }
    pagecontent += "</tr>";
      
        });
//
    pagecontent += "</tbody>";
    pagecontent += "</table>";
    

   document.getElementById("pagespaceRC").innerHTML = pagecontent;
  
}

function  getAllRequestRC(nic,ward){
  
   var week                = $("#pagetable #week"+nic).val();
   var YearandMonth        = $("#pagetable #month"+nic).val();
   var yearandmonth        = YearandMonth.split("-");
   var year                = yearandmonth[0];
   var month               = yearandmonth[1];
   var monday              = $("#pagetable #monday"+nic).val();
   var tuesday             = $("#pagetable #tuesday"+nic).val();
   var wednesday           = $("#pagetable #wednesday"+nic).val();
   var thursday            = $("#pagetable #thursday"+nic).val();
   var friday              = $("#pagetable #friday"+nic).val();
   var saturday            = $("#pagetable #saturday"+nic).val();
   var sunday              = $("#pagetable #sunday"+nic).val();
   
   alert(ward+"-"+year+"-"+month+"-"+week+"-"+nic+"-"+monday+"-"+tuesday+"-"+wednesday+"-"+thursday+"-"+friday+"-"+saturday+"-"+sunday);
   

    if (year.length == 0 || month.length == 0 || week.length == 0 || nic.length == 0 || monday == "select" || tuesday == "select" || wednesday == "select" || thursday == "select" || friday == "select" || saturday == "select" || sunday.length == "select")
    {
        alert('Please Fill all the Fields in the Schedule');
    }
    else
    {
        
        $.ajax({
            url: 'http://127.0.0.1/NIMSINFO/index.php/work/addSchedule',
            type: 'POST',
            crossDomain: true,
            data: {"nic": nic,
                   "week":week,
                   "ward":ward,
                   "year":year,
                   "month":month,
                   "monday":monday,
                   "tuesday": tuesday,
                   "wednesday": wednesday, 
                   "thursday": thursday,
                   "friday":friday,
                   "saturday":saturday,
                   "sunday":sunday},
            success: function(data) {
                alert(data);
                //add a code here
            }
           });
    
    }
}

function getSceduleDetails()
{
    var week = $("#week option:selected").text();
    var YearandMonth = $("#month").val();
    var splitYearandMonth = YearandMonth.split("-");
    var year = splitYearandMonth[0];
    var month = splitYearandMonth[1];
    
        $.ajax({
            url: 'http://127.0.0.1/NIMSINFO/index.php/person/getSchedule',
            type: 'POST',
            crossDomain: true,
            data: {"week":week,
                   "year":year,
                   "month":month},
            success: function(data) {
            var json_parsed = $.parseJSON(data);
            createViewTblRC(json_parsed);
            }
        });    

}


function createViewTblRC(json)
{
    

    var pagecontent = "";

    pagecontent = "<table class=table1 id=pagetable>";
    pagecontent += "<thead>";
    pagecontent += "<tr>";
    pagecontent += "<th>RecordID</th>";
    pagecontent += "<th>Monday</th>";
    pagecontent += "<th>Tuesday</th>";
    pagecontent += "<th>Wednesday</th>";
    pagecontent += "<th>Thursday</th>";
    pagecontent += "<th>Friday</th>";
    pagecontent += "<th>Saturday</th>";
    pagecontent += "<th>Sunday</th>";
    pagecontent += "</tr>";
    pagecontent += "</thead>";
    pagecontent += "<tbody>";
    
    var i=1;
    $.each(json, function(index,el) {
            // el = object in array
            // access attributes: el.Id, el.Name, etc
            //alert(el.drugs.dName);
            
    if(el.processed==null || el.processed==false)
    {
    pagecontent += "<tr>";
    pagecontent += "<td name=reqid"+i+" id=reqid"+i+">"+el.id+"</td>";
    pagecontent += "<td name=sr_val"+i+" id=sr_val"+i+" >"+el.monday+"</td>";
    pagecontent += "<td name=req_dname"+i+" id=req_dname"+i+" >"+el.tuesday+"</td>";
    pagecontent += "<td name=req_qty"+i+" id=req_qty"+i+" >"+el.wednesday+"</td>";
    pagecontent += "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.thursday+"</td>";
    pagecontent += "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.friday+"</td>";
    pagecontent += "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.saturday+"</td>";
    pagecontent += "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.sunday+"</td>";
    i++;    
    }
    pagecontent += "</tr>";
      
        });
//
    pagecontent += "</tbody>";
    pagecontent += "</table>";
    

   document.getElementById("pagespacegGC").innerHTML = pagecontent;
  
}