/*
 *This javascript file is used to handle the Nurses Details Updation  
 */


function UpdateNurse() {
          
    var id= document.getElementById("id").value;
    var name= document.getElementById("name").value;
    var address= document.getElementById("address").value;
    var nic= document.getElementById("nic").value; 
    var category = $('input:radio[name=category]:checked').val();
    var dob= document.getElementById("dob").value;
    var ward= document.getElementById("ward").value;
    var grade= document.getElementById("grade").value;
    var gender = $('input:radio[name=gender]:checked').val();
  
    /*Verification of valid NIC Number */
        var pattern = new RegExp(/^[0-9]{9}[vVxX]$/);
        var result = pattern.test(nic);
    /*Validate Birth Day*/
    var today = new Date();
    var nowyear = today.getFullYear();
    var yeardob = parseInt(dob.substring(6, 10));
     
   if (name.length === 0 || address.length === 0 || dob.length === 0 || nic.length === 0 || gender.length === 0  || category.length === 0 || ward.length === 0 || grade.length === 0)
    {
        alert('Please Fill all Required the Fields');
    }
    else
    {   
        if(result === false)
        {
          alert('Invalid NIC Number (Follow The Format:123456789V)');
        }
         else if((nowyear-yeardob)<=18)
         {
                alert('You Have Entered Invalid Birth Day');
         }
        else{
           
           dissableElement();
           document.getElementById("loader").style.display = "block";
        $.ajax({
            url: 'http://127.0.0.1/NIMSINFO/index.php/person/updatePerson',
            type: 'POST',
            crossDomain: true,
            data: {"id" :id, "name": name, "address": address, "nic": nic, "gender": gender,"dob":dob, "category": category, "ward": ward, "grade": grade},
            success: function(dat) {
                alert('Successfully Updated');
                document.getElementById("loader").style.display = "none";    
                enableElement();
            }
        });
        }
    }

}
function getWards()
{
    alert('asa');
  $.ajax({
        url: 'http://127.0.0.1/NIMSINFO/index.php/wards/getWardList',
        type: 'POST',
        crossDomain: true,
        success: function(data) {
            var json_parsed = $.parseJSON(data);
            for (var i = 0; i < json_parsed.length; i++) {
                var newOption = $('<option>');
                newOption.attr('value', json_parsed[i]['name']).text(json_parsed[i]['name']);
                $('#getnurse').append(newOption);
            }
        }
    });

}

function dissableElement()
{    
    document.getElementById("name").disabled = true;
    document.getElementById("address").disabled = true;
    document.getElementById("nic").disabled = true;
    document.getElementById("gender").disabled = true;
    document.getElementById("category").disabled = true;
    document.getElementById("dob").disabled = true;
    document.getElementById("ward").disabled = true;
    document.getElementById("grade").disabled = true;
}
function enableElement()
{    
    document.getElementById("name").disabled = false;
    document.getElementById("address").disabled = false;
    document.getElementById("nic").disabled = false;
    document.getElementById("gender").disabled = false;
    document.getElementById("category").disabled = false;
    document.getElementById("dob").disabled = false;
    document.getElementById("ward").disabled = false;
    document.getElementById("grade").disabled = false;
}
function getGender()
{   var selectedGender;
     if (document.getElementById('genderM').checked) {
     selectedGender = document.getElementById('genderM').value;
    }
    else {
     selectedGender = document.getElementById('genderF').value;
    }
    return selectedGender;
}






