window.onload = getWards();
var CalcalatedTotalOTHours=0;
function getWards()
{
   $.ajax({
        url: 'http://127.0.0.1/NIMSINFO/index.php/person/getWardList',
        type: 'POST',
        crossDomain: true,
        success: function(data) {
           
            var json_parsed = $.parseJSON(data);
            for (var i = 0; i < json_parsed.length; i++) {
                var newOption = $('<option>');
                newOption.attr('value', json_parsed[i]['ward']).text(json_parsed[i]['ward']);
                $('#ward').append(newOption);
            }
        }
    });

}



function getNursesByWard()
{
    
    var val;
    val = $("#ward option:selected").text();
    
    document.getElementById("name").options.length = 0;
    $.ajax({
        url: 'http://127.0.0.1/NIMSINFO/index.php/work/getNurseDetailsByWard/' + val,
        type: 'POST',
        crossDomain: true,
        success: function(data) {
           var json_parsed = $.parseJSON(data);
            for (var i = 0; i < json_parsed.length; i++) {
                var newOption = $('<option>');
                var name = json_parsed[i]['name'];
                var nic = json_parsed[i]['nic'];
                var grade = json_parsed[i]['grade'];
                var namenic = name+"-"+nic+"-"+grade;
                newOption.attr('value',namenic).text(namenic);
                $('#name').append(newOption);
            }
        }
    });
}


function getNames()
{
    $.ajax({
        url: 'http://localhost/health/index.php/overtime/getNurseList',
        type: 'POST',
        crossDomain: true,
        success: function(data) {
            
            var json_parsed = $.parseJSON(data);
            for (var i = 0; i < json_parsed.length; i++) {
                var newOption = $('<option>');
                newOption.attr('value', json_parsed[i]['name']).text(json_parsed[i]['name']);
                $('#getnurse').append(newOption);
            }
        }
    });

}

function getAlert()
{
    
    var fromduration = 0;
    var toduration = 0;
    var hours = 0;
    
    // get the shift start time and date  
    var startdateandtime = $('#starttime').val();
    var startdateandtime = startdateandtime.split("-");
    var startdate  = startdateandtime[0];
    var fromdatemonthyear = startdate.split(" ");
    var fromdate = fromdatemonthyear[0];
    var frommonth = fromdatemonthyear[1];
    var fromyear = fromdatemonthyear[2];
    
    
    //get the shift end time and date
    var enddateandtime = $('#endtime').val();
    var enddateandtime = enddateandtime.split("-");
    var enddate = enddateandtime[0];
    var todatemonthyear = enddate.split(" ");
    var todate = todatemonthyear[0];
    var tomonth = todatemonthyear[1];
    var toyear = todatemonthyear[2];
    
    
    
    
    var ward = $("#ward option:selected").text();
    var namenic = $("#name option:selected").text();
    var splitNameNic = namenic.split("-");
    var name = splitNameNic[0];
    var nic = splitNameNic[1];

    var start = new Date(startdateandtime);

    var end = new Date(enddateandtime);
    
    if(startdateandtime.length == 0||enddateandtime.length == 0 || ward.length == 0|| namenic.length == 0){
            
            alert("Please Enter all the values");
    }
    else{
    if (end.getFullYear() === start.getFullYear()) {
        if (end.getMonth() === start.getMonth()) {
            if (end.getDate() === start.getDate()) {
                
                        if (end.getHours() === start.getHours()) {
                                alert("hours and dates are equal check the date");
                        
                         } else if (end.getHours() > start.getHours()) {
                                
                                //no of hours will be calculated
                                //get start hours
                                var starttime = start.getHours();
                            
                                //get end hours
                                var endtime = end.getHours();
                            
                                //no of hours willbe calculated
                                var numberofhoursstart = start.getHours();
                                var numberofhoursend = end.getHours();
                                var totalhours = numberofhoursend - numberofhoursstart;
                           
                                //assigning to variables
                                fromduration = numberofhoursstart;
                                toduration = numberofhoursend; 
                                hours = totalhours;
                                CalcalatedTotalOTHours =totalhours;
                               // alert("Total Hours "+totalhours);
                            
                                //send data to controller using addovertimetocontroller method
                                addOvertimeToController(name,nic,ward,fromduration,toduration,starttime,endtime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours);
                                
                         } else if (end.getHours() < start.getHours()) {
                    
                                alert("wrong input start hours are greater than end hours");
                         
                         }
                          
            } else if (end.getDate() > start.getDate()) {
             
                        if(end.getDate() === (start.getDate()+1)){
                            
                            //get start hours
                            var starttime = start.getHours();
                            //get end hours
                            var endtime = end.getHours();
                            //no of hours willbe calculated
                            var numberofhoursstart = 24 - start.getHours();
                            var numberofhoursend = end.getHours() - 0;
                            var totalhours = numberofhoursstart + numberofhoursend;
                            //assigning to variables
                            fromduration = numberofhoursstart;
                            toduration = numberofhoursend; 
                            hours = totalhours;
                            CalcalatedTotalOTHours =totalhours;
                         //   alert("Calculated OT Hours "+totalhours);
                            //send data to controller using addovertimetocontroller method
                            addOvertimeToController(name,nic,ward,fromduration,toduration,starttime,endtime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours);
                            
                        }
                        else{
                            alert("you cannot enter more than 2 days of overtime");
                        }
            
            }else if (end.getDate() < start.getDate()){
                            
                            alert("end date is smaller than start date of the same month of the same year");
            }
            
        } else if (end.getMonth() > start.getMonth()) {
            
                        alert("end month greater than start month");
                        if((end.getDate() === 1)&&(start.getDate() === 31)){
                            
                            //get start hours
                            var starttime = start.getHours();
                            //get end hours
                            var endtime = end.getHours();
                            //no of hours willbe calculated
                            var numberofhoursstart = 24 - start.getHours();
                            var numberofhoursend = end.getHours() - 0;
                            var totalhours = numberofhoursstart + numberofhoursend;
                            //assigning to variables
                            fromduration = numberofhoursstart;
                            toduration = numberofhoursend; 
                            hours = totalhours;
                            alert(totalhours+"End Month greater than StartMonth");
                            //send data to controller using addovertimetocontroller method
                            addOvertimeToController(name,nic,ward,fromduration,toduration,starttime,endtime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours);
                        
                        }
                        else if((end.getDate() === 1)&&(start.getDate() === 30)){
                            
                            //get start hours
                            var starttime = start.getHours();
                            //get end hours
                            var endtime = end.getHours();
                            //no of hours willbe calculated
                            var numberofhoursstart = 24 - start.getHours();
                            var numberofhoursend = end.getHours() - 0;
                            var totalhours = numberofhoursstart + numberofhoursend;
                            //assigning to variables
                            fromduration = numberofhoursstart;
                            toduration = numberofhoursend; 
                            hours = totalhours;
                            alert(totalhours+"End Month greater than StartMonth");
                            //send data to controller using addovertimetocontroller method
                            addOvertimeToController(name,nic,ward,fromduration,toduration,starttime,endtime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours);
                        
                        }
                        else if((end.getDate() === 1)&&(start.getDate() === 29)){
                            
                            //get start hours
                            var starttime = start.getHours();
                            //get end hours
                            var endtime = end.getHours();
                            //no of hours willbe calculated
                            var numberofhoursstart = 24 - start.getHours();
                            var numberofhoursend = end.getHours() - 0;
                            var totalhours = numberofhoursstart + numberofhoursend;
                            //assigning to variables
                            fromduration = numberofhoursstart;
                            toduration = numberofhoursend; 
                            hours = totalhours;
                            alert(totalhours+"End Month greater than StartMonth");
                            //send data to controller using addovertimetocontroller method
                            addOvertimeToController(name,nic,ward,fromduration,toduration,starttime,endtime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours);
                        
                        }
                        else if((end.getDate() === 1)&&(start.getDate() === 28)){
                            
                            //get start hours
                            var starttime = start.getHours();
                            //get end hours
                            var endtime = end.getHours();
                            //no of hours willbe calculated
                            var numberofhoursstart = 24 - start.getHours();
                            var numberofhoursend = end.getHours() - 0;
                            var totalhours = numberofhoursstart + numberofhoursend;
                            //assigning to variables
                            fromduration = numberofhoursstart;
                            toduration = numberofhoursend; 
                            hours = totalhours;
                            CalcalatedTotalOTHours =totalhours;
                            alert(totalhours+"End Month greater than StartMonth");
                            //send data to controller using addovertimetocontroller method
                            addOvertimeToController(name,nic,ward,fromduration,toduration,starttime,endtime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours);
                        
                        
                        }
                        else{
                            alert("you cannot have more than 24 hours");
                        }
     } else if (end.getMonth() < start.getMonth()) {
            
                            alert("start month greater than end month");
        }
    } else if (end.getFullYear() > start.getFullYear()) {
                        
                        if((end.getMonth() === 0)&& (start.getMonth() === 11)){
                        
                            if(end.getDate() === 1 && start.getDate() === 31){
                            alert("end year greater than start year");
                            
                            // no of hours will be calculated
                            //get start hours
                            var starttime = start.getHours();
                            //get end hours
                            var endtime = end.getHours();
                            //no of hours willbe calculated
                            var numberofhoursstart = 24 - start.getHours();
                            var numberofhoursend = end.getHours() - 0;
                            var totalhours = numberofhoursstart + numberofhoursend;
                            //assigning to variables
                            fromduration = numberofhoursstart;
                            toduration = numberofhoursend; 
                            hours = totalhours;
                            CalcalatedTotalOTHours =totalhours;
                            alert(totalhours+"End Month greater than StartMonth");
                            //send data to controller using addovertimetocontroller method
                            addOvertimeToController(name,nic,ward,fromduration,toduration,starttime,endtime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours);
                       
                            }else{
                                alert("you cannot have more than 24 hours check the two dates");
                            }
                            
                        }else{
                            alert("you cannot have more than one month differnece");
                        }


    } else if (end.getFullYear() < start.getFullYear()) {
        
                        alert("wrong Date input Please check the year");
    }
   
    }      
}

function addOvertimeToController(name,nic,ward,fromduration,toduration,fromtime,totime,fromdate,frommonth,fromyear,todate,tomonth,toyear,hours){
 
  //  alert(name+""+nic+""+ward+""+fromduration+""+toduration+""+fromtime+""+totime+""+fromdate+""+frommonth+""+fromyear+""+todate+""+tomonth+""+toyear+""+hours);

        if(parseInt(hours) <= 6){
            $.ajax({
            url: 'http://127.0.0.1/NIMSINFO/index.php/overtime/addOvertime',
            type: 'POST',
            crossDomain: true,
            data: {"name": name, 
                   "nic": nic,
                   "ward":ward,
                   "fromduration":fromduration,
                   "toduration":toduration,
                   "fromtime":fromtime,
                   "totime":totime,
                   "fromdate": fromdate,
                   "frommonth":frommonth,
                   "fromyear":fromyear,
                   "todate": todate,
                   "tomonth":tomonth,
                   "toyear":toyear,
                   "hours": hours},
            success: function(data) {
                alert(data);
                document.getElementById("starttime").value = "";
                document.getElementById("endtime").value = "";
            }
        });
        }else{
            alert("Overtime Hours Should be less than or equal to 6 hours \n"+"Calculated Total OT Hours = "+CalcalatedTotalOTHours);
        }
    
}

function clearovertimeAll(){
         
         document.getElementById("starttime").value = "";
         document.getElementById("endtime").value = "";
         
}

function clearpublicholidayandoffduty(){
         document.getElementById("offday").value = "";
         document.getElementById("publicholiday").value = "";
         document.getElementById("date").value = "";
}


function getRequestToViewRC() {

     $.ajax({
        url: 'http://localhost/health/index.php/overtime/getNurseList',
        type: 'POST',
        crossDomain: true,
        //data: {"myOrderString": myOrderString},  // fix: need to append your data to the call
        success: function (data) {
            data = trimData(data);
           var json_parsed = $.parseJSON(data);
           createRequestViewTblRC(json_parsed);      
        }
    });
}

function createRequestViewTblRC(json){


    var pagecontent = "";

    pagecontent = "<table class=table1 id=pagetable>";
    pagecontent += "<thead>";
    pagecontent += "<tr>";
    pagecontent += "<th>Request ID</th>";
    pagecontent += "<th>Name</th>";
    pagecontent += "<th>NIC</th>";
    pagecontent += "<th>Month</th>";
    pagecontent += "<th>Week</th>";
    pagecontent += "<th>Monday</th>";
    pagecontent += "<th>Tuesday</th>";
    pagecontent += "<th>Wednesday</th>";
    pagecontent += "<th>Thursday</th>";
    pagecontent += "<th>Friday</th>";
    pagecontent += "<th>Saturday</th>";
    pagecontent += "<th>Sunday</th>";
    pagecontent += "</tr>";
    pagecontent += "</thead>";
    pagecontent += "<tbody>";
    
    var i=1;
    $.each(json, function(index,el) {
            // el = object in array
            // access attributes: el.Id, el.Name, etc
            //alert(el.drugs.dName);
            
    if(el.processed==null || el.processed==false)
    {
    pagecontent += "<tr>";
    pagecontent += "<td name=reqid"+i+" id=reqid"+i+">"+el.id+"</td>";
    pagecontent += "<td name=sr_val"+i+" id=sr_val"+i+" >"+el.name+"</td>";
    pagecontent += "<td name=req_dname"+i+" id=req_dname"+i+" >"+el.address+"</td>";
    pagecontent += "<td name=req_qty"+i+" id=req_qty"+i+" >"+el.nic+"</td>";
    pagecontent += "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.gender+"</td>";
    pagecontent += "<td>"+el.requestedDate+"</td>";
    pagecontent += "<td>"+el.department+"</td>";

    
    pagecontent += "<td>Pending</td>";
    pagecontent += "<td><input type=date name=appQtyValueRC"+i+" id=appQtyValueRC"+i+" value='"+el.quantity+"'/></td>";
    pagecontent += "<td><input type=checkbox name=chkRequestValueRC"+i+" id=chkRequestValueRC"+i+" /></td>";
    i++;    
    }
    pagecontent += "</tr>";
      
        });
//
    pagecontent += "</tbody>";
    pagecontent += "</table>";
    

   document.getElementById("pagespaceRC").innerHTML = pagecontent;
  
}





function getOvertimeToView() {

     var ward = $("#ward option:selected").text();
     var namenic = $("#name option:selected").text();
     var splitNameNic = namenic.split("-");
     var name = splitNameNic[0];
     var nic = splitNameNic[1];
   
     
     var YearandMonth = $("#month").val();
     var splitYearandMonth = YearandMonth.split("-");
     
     var year = splitYearandMonth[0];
     var monthnumber = splitYearandMonth[1];
     var month = "";
     if(monthnumber === '01'){
         month = 'January';
     }else if(monthnumber === '02'){
         month = 'February';
     }else if(monthnumber === '03'){
         month ='March';
     }else if(monthnumber === '04'){
         month ='April';
     }else if(monthnumber === '05'){
         month ='May';
     }else if(monthnumber === '06'){
         month ='June';
     }else if(monthnumber ===  '07'){
         month ='July';
     }else if(monthnumber === '08'){
         month ='August';
     }else if(monthnumber === '09'){
         month ='September';
     }else if(monthnumber === '10'){
         month ='Octomber';
     }else if(monthnumber === '11'){
         month ='November';
     }else if(monthnumber === '12'){
         month ='December';
     }

     if(namenic.length == 0 || YearandMonth.length == 0){
         alert("Please Select all the Fields");
     }
     else{
     $.ajax({
        url: 'http://127.0.0.1/NIMSINFO/index.php/overtime/getOvertime',
        type: 'POST',
        crossDomain: true,
        data: {"ward": ward,"year":year,"nic":nic,"month":month}, 
        success: function (data) {
            data = trimData(data);
            var json_parsed = $.parseJSON(data);
            createOvertimeViewTbl(json_parsed);
        }
    });
    }
}

function getPublicHolidaysandOffDaysToView(){
     
     var totalhours = $('#tothours').val();
     var ward = $("#ward option:selected").text();
     var namenic = $("#name option:selected").text();
     var splitNameNic = namenic.split("-");
     var name = splitNameNic[0];
     var nic = splitNameNic[1];
   
     
     var YearandMonth = $("#month").val();
     var splitYearandMonth = YearandMonth.split("-");
     
     var year = splitYearandMonth[0];
     var month = splitYearandMonth[1];
    
     if(namenic.length == 0 || YearandMonth.length == 0 || totalhours.length == 0){
         alert("Please Select  the Fields");
     }
     else{ 
     $.ajax({
        url: 'http://127.0.0.1/NIMSINFO/index.php/overtime/getOffDutyandPublicHolidayDuty',
        type: 'POST',
        crossDomain: true,
        data: {"ward": ward,"year":year,"nic":nic,"month":month}, 
        success: function (data) {
            data = trimData(data);
           var json_parsed = $.parseJSON(data);
           createOffDutyAndPublicHolidayDutyViewTbl(json_parsed);      
        }
    });  
     }
}


function createOffDutyAndPublicHolidayDutyViewTbl(json_parsed){
    var pagecontent = "";
    
    pagecontent = "<table class=table1 id=tablecontent>";
    pagecontent += "<thead>";
    pagecontent += "<tr>";
    pagecontent += "<th>NO</th>";
    pagecontent += "<th>NIC</th>";
    pagecontent += "<th>Date</th>";
    pagecontent += "<th>Off Days</th>";
    pagecontent += "<th>Public Holiday</th>";
    pagecontent += "</tr>";
    pagecontent += "</thead>";
    pagecontent += "<tbody>";
    
    var i=1;
    var totaloffdays = 0;
    var offday = 0;
    
    var totalpublicholidays = 0;
    var publicholiday = 0;
    
    $.each(json_parsed, function(index,el) {
            // el = object in array
            // access attributes: el.Id, el.Name, etc
            //alert(el.drugs.dName);
    
    if(el.processed==null || el.processed==false)
    {            
    pagecontent += "<tr>";
    pagecontent += "<td name=reqid"+i+" id=reqid"+i+">"+i+"</td>";
    pagecontent += "<td name=req_qty"+i+" id=req_qty"+i+" >"+el.nic+"</td>";
    pagecontent += "<td name=req_dname"+i+" id=req_dname"+i+" >"+el.date+"-"+el.month+"-"+el.year+"</td>";
    pagecontent += "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.offday+"</td>";
    pagecontent +=  "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.publicholiday+"</td>";
    i++;
    offday =  el.offday;
    publicholiday = el.publicholiday;
    }
    pagecontent += "</tr>";
    totaloffdays = totaloffdays + offday;   
    totalpublicholidays = totalpublicholidays + publicholiday;
        });
//
    pagecontent += "</tbody>";
    pagecontent += "</table>";
  
    

   document.getElementById("pagespaceAC").innerHTML = pagecontent;
   document.getElementById("totaloffdays").value = totaloffdays;
   document.getElementById("totalpublicholidays").value = totalpublicholidays;
}

function clearAll(){
    document.getElementById("pagespaceAC").innerHTML = "";
    document.getElementById("pagespaceOC").innerHTML = "";
    document.getElementById("totaloffdays").value = "";
    document.getElementById("totalpublicholidays").value = "";
    document.getElementById("tothours").value = "";
    document.getElementById("overtimerate").value = "";
    document.getElementById("publicholidayrate").value = "";
    document.getElementById("rateforoffday").value = "";
    
    document.getElementById("overtimecalculation").value = "";
    document.getElementById("paymentforoffdays").value = "";
    document.getElementById("paymentforpublicholidays").value = "";
    document.getElementById("totalpayment").value = "";
    
}

function createOvertimeViewTbl(json){


    var pagecontent = "";

    pagecontent = "<table class=table1 id=pagetable>";
    pagecontent += "<thead>";
    pagecontent += "<tr>";
    pagecontent += "<th>No</th>";
    pagecontent += "<th>NIC</th>";
    pagecontent += "<th>From</th>";
    pagecontent += "<th>Overtime Commence</th>";
    pagecontent += "<th>To</th>";
    pagecontent += "<th>Overtime End</th>";
    pagecontent += "<th>Overtime Hours</th>";
    pagecontent += "</tr>";
    pagecontent += "</thead>";
    pagecontent += "<tbody>";
    
    var i=1;
    var totalhours = 0;
    var hours = 0;
    var timestring = "";
    
    $.each(json, function(index,el) {
            // el = object in array
            // access attributes: el.Id, el.Name, etc
            //alert(el.drugs.dName);
    
    if(el.processed==null || el.processed==false)
    {
    if(parseInt(el.fromtime) < 12){
        timestring = "AM";
    }else{
        timestring = "PM";
    }
    
    if(parseInt(el.totime) < 12){
        timestring = "AM";
    }else{
        timestring = "PM";
    }
    pagecontent += "<tr>";
    pagecontent += "<td name=reqid"+i+" id=reqid"+i+">"+i+"</td>";
    pagecontent += "<td name=req_qty"+i+" id=req_qty"+i+" >"+el.nic+"</td>";
    pagecontent += "<td name=req_dname"+i+" id=req_dname"+i+" >"+el.fromdate+"-"+el.frommonth+"-"+el.fromyear+"</td>";
    pagecontent += "<td name=fromtime"+i+" id=fromtime"+i+" >"+el.fromtime+""+timestring+"</td>";
    pagecontent += "<td name=avail_qty"+i+" id=avail_qty"+i+" >"+el.todate+"-"+el.tomonth+"-"+el.toyear+"</td>";
    pagecontent += "<td name=fromtime"+i+" id=fromtime"+i+" >"+el.totime+""+timestring+"</td>";
    pagecontent += "<td>"+el.hours+"</td>";
    i++;
    hours =  el.hours;
    }
    pagecontent += "</tr>";
    
    totalhours = totalhours + hours;   
        });
//
    pagecontent += "</tbody>";
    pagecontent += "</table>";
   
    

   document.getElementById("pagespaceOC").innerHTML = pagecontent;
   document.getElementById("tothours").value = totalhours;
   
}



function calculateOvertime(){
    
     var namenicgrade = $('#name').val();
     var totalhours = $('#tothours').val();
     var splitNameNicGrade = namenicgrade.split("-");
     var name = splitNameNicGrade[0];
     var nic = splitNameNicGrade[1];
     var grade = splitNameNicGrade[2];
     var overtimecalculation;
     var calculationforoffdays;
     var calculationforpublicholidays;
     var totalpayment;
     
     var totalpublicholidays = $('#totalpublicholidays').val();
     var totaloffdays        = $('#totaloffdays').val();
     
     if(namenicgrade.length == 0 || totalhours.length == 0 || totalpublicholidays.length == 0 || totaloffdays.length == 0){
         alert("Please Select all the Fields");
     }
     else{
    $.ajax({
        url: 'http://127.0.0.1/NIMSINFO/index.php/grade/getGradeInfo/' + grade,
        type: 'POST',
        crossDomain: true,
        success: function(data) {
                var json_parsed = $.parseJSON(data);
                var hourslimit = json_parsed[0]['hourslimit'];
                var overtimerate = json_parsed[0]['overtimerate'];
                var offdutyrate  = json_parsed[0]['offdutyrate'];
                var publicholidayrate = json_parsed[0]['publicholidayrate'];
                
                document.getElementById("overtimerate").value = overtimerate;
                document.getElementById("publicholidayrate").value = publicholidayrate;
                document.getElementById("rateforoffday").value = offdutyrate;
                
                
                
                if(totalhours < hourslimit){
                    document.getElementById("overtimecalculation").value = "Total Overtime Hours are less than Limit";
                    
                    calculationforoffdays = offdutyrate  * totaloffdays;
                    document.getElementById("paymentforoffdays").value = calculationforoffdays;
                
                    calculationforpublicholidays = publicholidayrate * totalpublicholidays;
                    document.getElementById("paymentforpublicholidays").value = calculationforpublicholidays;
               
                    totalpayment = calculationforoffdays + calculationforpublicholidays;
                    document.getElementById("totalpayment").value = totalpayment;
               
                }else{
                    overtimecalculation = overtimerate  * totalhours;
                    document.getElementById("overtimecalculation").value = overtimecalculation;
                    
                    calculationforoffdays = offdutyrate  * totaloffdays;
                    document.getElementById("paymentforoffdays").value = calculationforoffdays;
                
                    calculationforpublicholidays = publicholidayrate * totalpublicholidays;
                    document.getElementById("paymentforpublicholidays").value = calculationforpublicholidays;
                    
                    totalpayment = overtimecalculation + calculationforoffdays + calculationforpublicholidays; 
                    document.getElementById("totalpayment").value = totalpayment;
               
                }
                
                
        }
    });
    }
}

function getpublicandoffduty(){
    
    var ward = $("#ward").val();
    var NameNicGrade = $("#name").val();
    var MonthDateYear = $("#date").val();
    var offday = $("#offday").val();
    var publicholiday = $("#publicholiday").val();
    
    var splitMonthDateYear = MonthDateYear.split("-");
    var year = splitMonthDateYear[0];
    var month  = splitMonthDateYear[1];
    var date  = splitMonthDateYear[2];
    
    
    var splitNameNicGrade = NameNicGrade.split("-");
    var name = splitNameNicGrade[0];
    var nic  = splitNameNicGrade[1];
    var grade = splitNameNicGrade[2];
    
    if(ward.length == 0 || name.length == 0 || nic.length == 0|| grade.length == 0 || offday.length == 0 || publicholiday.length == 0 || year.length == 0 || month.length == 0 || date.length == 0){
        alert("Please Enter all the Fields");
    }
    else{
         $.ajax({
            url: 'http://127.0.0.1/NIMSINFO/index.php/overtime/addPublicHolidayAndOffDay',
            type: 'POST',
            crossDomain: true,
            data: {"name": name, 
                   "nic": nic,
                   "grade":grade,
                   "ward":ward,
                   "year":year,
                   "date": date,
                   "month":month,
                   "offday":offday,
                   "publicholiday": publicholiday},
            success: function(data) {
                alert(data);
                clearpublicholidayandoffduty();
            }
        });
    }
}