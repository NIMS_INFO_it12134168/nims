<?php
class Overtime extends CI_Controller {

    public function index() {
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->view('overtime',$data);
    }
    
     public  function getNurseList()
    {
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HIS/rest/registration/getNurseNames');
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        echo json_encode($_resultsAfterDecode);   
    }
    
    
    public function addOvertime(){

        $_nic = $this->input->post('nic');
        $_ward = $this->input->post('ward');
        $_name = $this->input->post('name');
        
        $_fromdate = $this->input->post('fromdate');
        $_frommonth = $this->input->post('frommonth');
        $_fromyear = $this->input->post('fromyear');
                
        $_todate = $this->input->post('todate');
        $_tomonth = $this->input->post('tomonth');
        $_toyear = $this->input->post('toyear');
        
        $_fromtime = $this->input->post('fromtime');
        $_totime = $this->input->post('totime');
        $_hours = $this->input->post('hours');
        $_fromduration = $this->input->post('fromduration');
        $_toduration = $this->input->post('toduration');
        
    
        $_serviceUrl = "http://localhost:8080/HISWS/rest/overtime/insert";


        $_curl = curl_init($_serviceUrl);

        $person = array('nic' => $_nic,
                        'name' => $_name,
                        'ward' => $_ward,
                        
                        'fromdate' => $_fromdate,
                        'frommonth' => $_frommonth,
                        'fromyear' => $_fromyear, 
                        'fromtime' => $_fromtime,
                        
                        'todate' => $_todate,
                        'tomonth' => $_tomonth,
                        'toyear' => $_toyear,
                        'totime' => $_totime,
            
                        'hours' => $_hours,
                        'fromduration' => $_fromduration,
                        'toduration' => $_toduration);
        
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
        
        
    }
    
    public function OvertimeSummary(){
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->view('overtimesummary',$data);
    }
    
    public function getOvertime(){
        
        
        $_year = $this->input->post('year');
        $_nic = $this->input->post('nic');
        $_month = $this->input->post('month');
        $_ward = $this->input->post('ward');


        $_serviceUrl = "http://localhost:8080/HISWS/rest/overtime/getOvertime";


        $_curl = curl_init($_serviceUrl);

        $person = array('year' => $_year,
                        'nic' => $_nic,
                        'month' => $_month,
                        'ward' => $_ward);

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        $_resultsAfterDecode = json_decode( $_result);
        echo json_encode($_resultsAfterDecode);
  }

  public function getOffDutyandPublicHolidayDuty(){
        $_year = $this->input->post('year');
        $_nic = $this->input->post('nic');
        $_month = $this->input->post('month');
        $_ward = $this->input->post('ward');


        $_serviceUrl = "http://localhost:8080/HISWS/rest/publicholidayandoffduty/getpublicholidayandoffdayduty";


        $_curl = curl_init($_serviceUrl);

        $person = array('year' => $_year,
                        'nic' => $_nic,
                        'month' => $_month,
                        'ward' => $_ward);

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        $_resultsAfterDecode = json_decode( $_result);
        echo json_encode($_resultsAfterDecode);
  }
  
  
  
    public function getOvertimeInfo($_category)
    {
        
        $this->load->library('curl');
        $_catAfterStrReplace = str_replace(' ', '%20', $_category);
        $_resultFromService = $this->curl->simple_get(
            'http://localhost:8080/HIS/rest/overtime/getWards/' .  $_catAfterStrReplace
        );

        $_resultsAfterDecode = json_decode($_resultFromService);
        echo json_encode($_resultsAfterDecode);
        
    }
    
    
    public function publicHolidayAndOffduty(){
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->view('publicandoffduty',$data);
    }
    
     function getImage() {
        $this->load->library('session');
        $uid = $this->session->userdata('username');
        $path = "";
        
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/profile/getProfileImage/' . $uid
        );
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        foreach ($_resultsAfterDecode as $mypath) {
            $path = $mypath[1];
        }
        $path = 'uploads/' . $path;
        return $path;
    }
    
    
    
    public function addPublicHolidayAndOffDay(){
        
        $_nic = $this->input->post('nic');
        $_ward = $this->input->post('ward');
        $_name = $this->input->post('name');
        $_grade = $this->input->post('grade');
        
        
        $_date = $this->input->post('date');
        $_month = $this->input->post('month');
        $_year = $this->input->post('year');
        $_offday = $this->input->post('offday');
        $_publicholiday = $this->input->post('publicholiday');
                
        
       
       
    
        $_serviceUrl = "http://localhost:8080/HISWS/rest/publicholidayandoffduty/insert";


        $_curl = curl_init($_serviceUrl);

        $person = array('nic' => $_nic,
                        'name' => $_name,
                        'ward' => $_ward,
                        'grade'=> $_grade,
                        'date' => $_date,
                        'month' => $_month,
                        'year' => $_year,
                        'offday' => $_offday,
                        'publicholiday' => $_publicholiday);
        
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
        
    }
    
    
    
}
?>