<?php
class Leaves extends CI_Controller {

	// num of records per page
	private $limit = 10;
	
	function Leaves(){
		//parent::Controller();
		
		// load library
		$this->load->library(array('table','validation'));
		
		// load helper
		//$this->load->helper('url');
		
		// load model
		$this->load->model('requsetModel','',TRUE);
	}
	
	function index($offset = 0){
		// offset
		$uri_segment = 3;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$persons = $this->requsetModel->get_paged_list($this->limit, $offset)->result();
		
		// generate pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url('leave/index/');
 		$config['total_rows'] = $this->requestModel->count_all();
 		$config['per_page'] = $this->limit;
		$config['uri_segment'] = $uri_segment;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		// generate table data
		$this->load->library('table');
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Name', 'Ward','Category', 'Actions');
		$i = 0 + $offset;
		foreach ($persons as $person){
			$this->table->add_row(++$i, $person->Name,$person->ApplyDate,$person->Ward,$person->Grade,
				anchor('person/view/'.$person->id,'view',array('class'=>'view')).' '.
				anchor('person/update/'.$person->id,'update',array('class'=>'update')).' '.
				anchor('person/delete/'.$person->id,'delete',array('class'=>'delete','onclick'=>"return confirm('Are you sure want to delete this person?')"))
			);
		}
		$data['table'] = $this->table->generate();
		
		// load view
		$this->load->view('personList', $data);
	}
	
	function leave(){
		// set validation properties
		$this->_set_fields();
		
		// set common properties
		$data['title'] = 'Nurses Leave Request';
		$data['message'] = '';
		$data['action'] = site_url('leave/addLeave');
		$data['link_back'] = anchor('leave/leave/','Back to list of Nurses',array('class'=>'back'));
	
		// load view
		$this->load->view('leaveform', $data);
	}
	function addLeave(){
		// set common properties
		$data['title'] = 'Nurses Leave Request';
		$data['action'] = site_url('leave/addLeave');
		//$data['link_back'] = anchor('leave/index/','Back to list of Nurses',array('class'=>'back'));
		
		// set validation properties
		$this->_set_fields();
		$this->_set_rules();
		
		// run validation
		if ($this->validation->run() == FALSE){
			$data['message'] = '';
		}else{
			// save data
			$person = array('Name' => $this->input->post('name'),
							'ApplyDate'=>$this->input->post('txtDate'),
							'LeaveType' => $this->input->post('leavetype'),
							'LeaveCommence' => date('Y-m-d', strtotime($this->input->post('dob'))),
							'NoLeaves' => $this->input->post('nleaves'),
							'Ward' => $this->input->post('ward'),
							'Grade' => $this->input->post('grade'),
							);
			$id = $this->requestModel->save($person);
			
			// set form input name="id"
			$this->validation->id = $id;
			
			// set user message
			$data['message'] = '<div class="success">Transaction Successfully Completed</div>';
		}
		
		// load view
		$this->load->view('leaveform', $data);
	}
	
/*	function view($id){
		// set common properties
		$data['title'] = 'Nurses Details';
		$data['link_back'] = anchor('person/index/','Back to list of Nurses',array('class'=>'back'));
		
		// get person details
		$data['person'] = $this->personModel->get_by_id($id)->row();
		
		// load view
		$this->load->view('personView', $data);
	}
	
	function update($id){
		// set validation properties
		$this->_set_fields();
		
		// prefill form values
		$person = $this->personModel->get_by_id($id)->row();
		$this->validation->id = $id;
		$this->validation->name = $person->Name;
		$this->validation->address = $person->Address;
		$this->validation->nic = $person->NIC;
		//$_POST['nic'] = $person->NIC;
		$_POST['gender'] = strtoupper($person->Gender);
		$this->validation->dob = date('d-m-Y',strtotime($person->DOB));
		
		// set common properties
		$data['title'] = 'Update Nurse';
		$data['message'] = '';
		$data['action'] = site_url('person/updatePerson');
		$data['link_back'] = anchor('person/index/','Back to list of Nurses',array('class'=>'back'));
	
		// load view
		$this->load->view('personEdit', $data);
	}
	
	function updatePerson(){
		// set common properties
		$data['title'] = 'Update Nurse';
		$data['action'] = site_url('person/updatePerson');
		$data['link_back'] = anchor('person/index/','Back to list of Nurses',array('class'=>'back'));
		
		// set validation properties
		$this->_set_fields();
		$this->_set_rules();
		
		// run validation
		if ($this->validation->run() == FALSE){
			$data['message'] = '';
		}else{
			// save data
			$id = $this->input->post('id');
			$person = array('Name' => $this->input->post('name'),
							'Address' => $this->input->post('address'),
							'NIC' => $this->input->post('nic'),
							'Ward' => $this->input->post('ward'),
							'Gender' => $this->input->post('gender'),
							'Category' => $this->input->post('category'),
							'Ward' => $this->input->post('ward'),
							'Grade' => $this->input->post('grade'),
							'DOB' => date('Y-m-d', strtotime($this->input->post('dob'))));
			$this->personModel->update($id,$person);
			
			// set user message
			$data['message'] = '<div class="success">Update Nurse Success</div>';
		}
		
		// load view
		$this->load->view('personEdit', $data);
	}
	
	function delete($id){
		// delete person
		$this->personModel->delete($id);
		
		// redirect to person list page
		redirect('person/index/','refresh');
	}
	*/
	// validation fields
	function _set_fields(){
		$fields['id'] = 'id';     
		$fields['name'] = 'name';
		$fields['address'] = 'address';
		$fields['gender'] = 'gender';
		$fields['nic'] = 'nic';
		$fields['dob'] = 'dob';
		
		$this->validation->set_fields($fields);
	}
	
	// validation rules
	function _set_rules(){
		$rules['name'] = 'trim|required';
		$rules['gender'] = 'trim|required';
	    $rules['address'] = 'trim|required';
		$rules['nic'] = 'trim|required';
		$rules['dob'] = 'trim|required|callback_valid_date';
		
		$this->validation->set_rules($rules);
		
		$this->validation->set_message('required', '* required');
		$this->validation->set_message('isset', '* required');
		$this->validation->set_error_delimiters('<p class="error">', '</p>');
	}
	
	// date_validation callback
	function valid_date($str)
	{
		if(!ereg("^(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-([0-9]{4})$", $str))
		{
			$this->validation->set_message('valid_date', 'date format is not valid. dd-mm-yyyy');
			return false;
		}
		else
		{
			return true;
		}
	}
}
?>