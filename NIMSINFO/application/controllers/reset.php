<?php
class Reset extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    function index() {
        $this->load->view('upload_form_admin', array('error' => ' ' ));
    }
	function reset_nurse() {
        $this->load->view('resetpassword', array('error' => ' ' ));
    }
	function reset_password() {
      $password=$this->input->post('pword');
	  $passconf=$this->input->post('cpword');
	  $this->load->library('session');
	
		$uid =   $this->session->userdata('username');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{	$data['action'] = site_url('reset/reset_password');
			$data['message'] = "";
			$this->load->view('resetpassword', $data);
		}
		else{
			$data = array(
               'Password' => $password
            );
			$this->resetModel->update($uid, $data);
          
			}
	}

}