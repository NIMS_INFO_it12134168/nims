<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Grade extends CI_Controller {

    public function index() {
        $this->load->view('grade');
    }
    
    public function insertGrade(){
        $_grade = $this->input->post('grade');
        $_bascisalary = $this->input->post('basicsalary');
        $_hourslimit = $this->input->post('hourslimit');
        $_overtimerate = $this->input->post('hoursrate');
        $_publicholidayrate = $this->input->post('publicholidayrate');
        $_offdutydayrate = $this->input->post('offdutydayrate');
        
        $_serviceUrl = "http://localhost:8080/HIS/rest/grade/insert";


        $_curl = curl_init($_serviceUrl);

        $person = array(
            'grade' => $_grade,
            'basicsalary' => $_bascisalary,
            'hourslimit' => $_hourslimit,
            'overtimerate' => $_overtimerate,
            'publicholidayrate' => $_publicholidayrate,
            'offdutyrate' => $_offdutydayrate);
        
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
    }
    
    public function getGradeInfo($_category){
        
        $this->load->library('curl');
        $_catAfterStrReplace = str_replace(' ', '%20', $_category);
        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/grade/getWards/' . $_catAfterStrReplace
        );

        $_resultsAfterDecode = json_decode($_resultFromService);
        echo json_encode($_resultsAfterDecode);
    }
    
    public  function UpdateGrade(){
        $this->load->view('gradeupdate');
    }
    
    public function getGrades(){
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HIS/rest/grade/getGrades');
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        echo json_encode($_resultsAfterDecode);
    }
    
    public function updateGradeToController(){
        $_id  = $this->input->post('id');
        $_grade = $this->input->post('grade');
        $_bascisalary = $this->input->post('basicsalary');
        $_hourslimit = $this->input->post('hourslimit');
        $_overtimerate = $this->input->post('hoursrate');
        $_publicholidayrate = $this->input->post('publicholidayrate');
        $_offdutydayrate = $this->input->post('offdutydayrate');
        
        $_serviceUrl = "http://localhost:8080/HIS/rest/grade/update";


        $_curl = curl_init($_serviceUrl);

        $person = array('id' => $_id,
            'grade' => $_grade,
            'basicsalary' => $_bascisalary,
            'hourslimit' => $_hourslimit,
            'overtimerate' => $_overtimerate,
            'publicholidayrate' => $_publicholidayrate,
            'offdutyrate' => $_offdutydayrate);
        
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
    }
    
}
?>
