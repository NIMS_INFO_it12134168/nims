<?php
class Upload extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    function index() {
        $this->load->view('upload_form_admin', array('error' => ' ' ));
    }
	function upload_nurse() {
        $this->load->view('upload_form_nurse', array('error' => ' ' ));
    }
	function do_upload_nurse() {
        $config = array(
            'upload_path'   => './uploads/',
            'allowed_types' => 'gif|jpg|png',
            'max_size'      => '2000',
            'max_width'     => '1024',
            'max_height'    => '768',
            'encrypt_name'  => false,
        );

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('upload_form_nurse', $error);
        } else {
            $upload_data = $this->upload->data();
			 $this->load->library('session');
			 $uid =   $this->session->userdata('username');
            $data_ary = array(
                //'title'     => $upload_data['client_name'],
                'file'      => $upload_data['file_name'],
                'width'     => $upload_data['image_width'],
                'height'    => $upload_data['image_height'],
                'type'      => $upload_data['image_type'],
                'size'      => $upload_data['file_size'],
		'NIC'		=> $uid,
                'date'      => time(),
            );
         $_serviceUrl = "http://localhost:8080/HISWS/rest/profile/uploadImage";


        $_curl = curl_init($_serviceUrl);

         $_dataString = json_encode($data_ary);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
           /* $this->load->database();
            $this->db->insert('upload', $data_ary);
			*/
           $data = array('upload_data' => $upload_data);
		   $data['result']='Successfully Uploades';
		   $data['uploadimage']='uploads/'.$upload_data['file_name'];
            $this->load->view('upload_success_nurse', $data);
        }
    }
    function do_upload() {
        $config = array(
            'upload_path'   => './uploads/',
            'allowed_types' => 'gif|jpg|png',
            'max_size'      => '2000',
            'max_width'     => '1024',
            'max_height'    => '768',
            'encrypt_name'  => false,
        );

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('upload_form_admin', $error);
        } else {
            $upload_data = $this->upload->data();
			 $this->load->library('session');
			 $uid =   $this->session->userdata('username');
            $data_ary = array(
                //'title'     => $upload_data['client_name'],
                'file'      => $upload_data['file_name'],
                'width'     => $upload_data['image_width'],
                'height'    => $upload_data['image_height'],
                'type'      => $upload_data['image_type'],
                'size'      => $upload_data['file_size'],
				'NIC'		=> $uid,
                'date'      => time(),
            );

          /*  $this->load->database();
            $this->db->insert('upload', $data_ary);
	*/
             $_serviceUrl = "http://localhost:8080/HISWS/rest/profile/uploadImage";


        $_curl = curl_init($_serviceUrl);

         $_dataString = json_encode($data_ary);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
           $data = array('upload_data' => $upload_data);
		   $data['result']='Successfully Uploades';
		   $data['uploadimage']='uploads/'.$upload_data['file_name'];
            $this->load->view('upload_successadmin', $data);
        }
    }
	function getImage()
	{
		$this->load->library('session');
		$uid =   $this->session->userdata('username');
		$path;
			$this->load->model('personModel','',TRUE);
		$persons = $this->personModel->get_by_nic_image($uid)->result();
			foreach ($persons as $person){
		        $path=$person->file;
			
			
		}
		
		
	    $path='uploads/'.$path;
		$data['title'] = 'Nurses Leaves Requests';
		$data['message'] = $uid;
		$data['filepath'] = $path;
		$data['link_back'] = anchor('person/loginadmin/','Back to list of Nurses',array('class'=>'back'));
	
		// load view
		$this->load->view('upload_success1', $data);

	}
}