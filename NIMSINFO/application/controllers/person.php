
<?php

class Person extends CI_Controller {

    // num of records per page
    private $limit = 10;
    //$GLOBALS['username'];d
    private $userrole;

    function Person() {
        parent::__construct();

        // load library
        $this->load->library(array('table', 'validation'));

        // load helper
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('javascript');
        // load model
        $this->load->model('personModel', '', TRUE);
    }

    //Display the requested leaves by nurses
    function leavelist() {
        // set validation properties
        $this->_set_fields();
        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment);

        // load data
        //$persons = $this->personModel->get_paged_list_leave($this->limit, $offset)->result();
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HISWS/rest/leaves/nursesLeaveTable');
        $persons = json_decode($_resultFromService, true);
        // generate pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url('person/leavelist/');
        $config['total_rows'] = $this->personModel->count_all_leave();
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $data['pagination'] = $this->pagination->create_links();

        // generate table data
        $this->load->library('table');
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('No', 'Name', 'Ward', 'Grade', 'Leave Commence', ' No Leaves', 'Actions');
        $i = 0 + $offset;
        foreach ($persons as $person) {
            //if($person->Result=='Pending'){
            $this->table->add_row(++$i, $person['name'], $person['ward'], $person['grade'], $person['leaveCommence'], $person['noLeaves'], anchor('person/updateLeave/' . $person['id'], 'View', array('class' => 'view')) . ' ' .
                    anchor('person/acceptLeave/' . $person['id'], 'Accept', array('class' => 'delete', 'onclick' => "return confirm('Are you sure want to Accept this Leave Request?')")) . ' ' .
                    anchor('person/rejectLeave/' . $person['id'], 'Reject', array('class' => 'delete', 'onclick' => "return confirm('Are you sure want to Reject this Leave Request?')"))
            );
            //}
        }
        $data['table'] = $this->table->generate();
        // set common properties
        $data['title'] = 'Nurses Leaves Requests';
        $data['message'] = '';

        $data['link_back'] = anchor('person/loginadmin/', 'Back to list of Nurses', array('class' => 'back'));

        // load view
        $this->load->view('leavelist', $data);
        ;
    }

    //This function represent the individual requested leaves by nurses
    function leaveHistory() {

        // set validation properties
        $this->_set_fields();
        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment);
        $this->load->library('session');
        $uid = $this->session->userdata('username');

        // load data

        $persons = $this->personModel->get_paged_list_leave_group($this->limit, $offset, $uid)->result();
        // generate pagination
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->library('pagination');
        $config['base_url'] = site_url('person/leaveHistory/');
        $config['total_rows'] = $this->personModel->count_all_leave_person($uid);
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();


        // generate table data
        $this->load->library('table');
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('No', 'Apply Date', 'Leave Commence', '#Leaves', 'Requset Result');
        $i = 0 + $offset;
        //$this->load->library('session');
        //$uid =   $this->session->userdata('username');
        foreach ($persons as $person) {

            $this->table->add_row(++$i, $person->ApplyDate, $person->LeaveCommence, $person->NoLeaves, $person->Result);
        }
        $data['table'] = $this->table->generate();
        // set common properties
        $data['title'] = 'Nurses Leaves Requests';
        $data['message'] = '';
        //$data['action'] = site_url('person/addLeave');
        $data['link_back'] = anchor('person/loginadmin/', 'Back to list of Nurses', array('class' => 'back'));

        // load view
        $this->load->view('leavehistory', $data);
    }

    function leave() {
        // set validation properties
        $this->_set_fields();

        // set common properties
        $data['title'] = 'Nurses Leaves';
        $data['message'] = '';
        $data['action'] = site_url('person/addLeave');
        $data['link_back'] = anchor('person/loginadmin/', 'Back to list of Nurses', array('class' => 'back'));

        // load view
        $this->load->view('leaveform', $data);
    }

    function addLeaveNurse() {
        /* set values to the web servicce */

        $_name = $this->input->post('name');
        $_applydate = $this->input->post('applydate');
        $_leavetype = $this->input->post('leavetype');
        $_ward = $this->input->post('ward');
        $_grade = $this->input->post('grade');
        $_commencedate = $this->input->post('commencedate');
        $_nodate = $this->input->post('nodate');
        $_reason = $this->input->post('reason');

        $this->load->library('session');
        $_nic = $this->session->userdata('username');

        $_serviceUrl = "http://localhost:8080/HISWS/rest/leaves/apply/";


        $_curl = curl_init($_serviceUrl);

        $person = array(
            'name' => $_name,
            'applydate' => $_applydate,
            'leavetype' => $_leavetype,
            'ward' => $_ward,
            'grade' => $_grade,
            'commencedate' => $_commencedate,
            'nodate' => $_nodate,
            'reason' => $_reason,
            'NIC' => $_nic
        );

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;

        // set common properties
        /* $data['title'] = 'Nurses Registration';
          $data['action'] = site_url('person/addLeaveNurse');
          $data['link_back'] = anchor('person/leaveHistory/','Back to list of Nurses',array('class'=>'back'));

          // set validation properties

          $this->_set_fieldsleave();
          $this->_set_rules();
          $this->load->library('session');
          $uid =   $this->session->userdata('username');


          $this->form_validation->set_rules('dob', 'Leave Commence Date ', 'required');
          $this->form_validation->set_rules('nleaves', 'Number of Leaves ', 'trim|required|is_natural_no_zero|exact_length[1]');
          $this->form_validation->set_rules('rleaving', 'Leaves Appling Reason ', 'trim|required');
          $this->form_validation->set_rules('txtDate', 'Leave Apply Date ', 'trim|required|exact_length[10]');
          $this->form_validation->set_rules('dob', 'Leave Commence Date ', 'trim|required|exact_length[10]');
          if ($this->form_validation->run() == FALSE)
          {	$data['message'] = '<div class="success">Registered Faild</div>';
          $myimage=$this->getImage();
          $data['image']=$myimage;

          $this->load->view('addleavenurse', $data);
          }else{
          // run validation
          $type=$this->input->post('ltype');
          $no=$this->input->post('nleaves');
          if((strcmp($type,"HalfDay")==0)&&($no !=1))
          {
          $data['message'] = '<div class="success">Extractly 1 HalfDay Can  Be Apply : Transaction Failed</div>';
          $myimage=$this->getImage();
          $data['image']=$myimage;

          $this->load->view('addleavenurse', $data);
          }
          // save data
          else{
          $person = array('Name' => $this->input->post('name'),
          'ApplyDate'=>$this->input->post('txtDate'),
          'LeaveType'=>$this->input->post('ltype'),
          'LeaveCommence' => date('Y-m-d', strtotime($this->input->post('dob'))),
          'NoLeaves' => $this->input->post('nleaves'),
          'Ward' => $this->input->post('ward'),
          'Grade' => $this->input->post('grade'),
          'Reason' =>$this->input->post('rleaving'),
          'NIC'=>$uid);
          //$id = $this->personModel->save1($person);
          $id = $this->personModel->save1($person);
          // set form input name="id"
          $this->validation->id = $id;

          // set user message
          $data['message'] = '<div class="success">Transaction Successfully Completed</div>';
          //$data['message'] = '<div class="success">Registered Successfully</div>';

          // load view
          $myimage=$this->getImage();
          $data['image']=$myimage;
          $this->load->view('addleavenurse', $data);
          }
          } */
    }
    
    public function LoadNurse()
    {   
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->view("Nurse" ,$data);
             
    }
            
    
    
    
    public function getSchedule()
    {   
        $this->load->library('session');
      
        $_nic = $this->session->userdata('username');
        $_week = $this->input->post('week');
        $_year = $this->input->post('year');
        $_month = $this->input->post('month');
                
        $_serviceUrl = "http://localhost:8080/HISWS/rest/work/getNurseSchedule";

        $_curl = curl_init($_serviceUrl);

        $person = array('nic' => $_nic,
                        'week' => $_week,
                        'year' => $_year,
                        'month' => $_month);

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        $_resultsAfterDecode = json_decode( $_result);
        echo json_encode($_resultsAfterDecode);
        
          
    }
    
    
    
    
    
    function addLeave() {
        // set common properties
        $data['title'] = 'Nurses Registration';
        $data['action'] = site_url('person/addLeave');
        $data['link_back'] = anchor('person/loginadmin/', 'Back to list of Nurses', array('class' => 'back'));

        // set validation properties
        $this->_set_fields();
        $this->_set_rules();
        $this->load->library('session');
        $uid = $this->session->userdata('username');
        // run validation
        // save data
        $person = array('Name' => $this->input->post('name'),
            'ApplyDate' => $this->input->post('txtDate'),
            'LeaveType' => $this->input->post('ltype'),
            'LeaveCommence' => date('Y-m-d', strtotime($this->input->post('dob'))),
            'NoLeaves' => $this->input->post('nleaves'),
            'Ward' => $this->input->post('ward'),
            'Grade' => $this->input->post('grade'),
            'Reason' => $this->input->post('rleaving'),
            'NIC' => $uid);
        //$id = $this->personModel->save1($person);
        $id = $this->personModel->save1($person);
        // set form input name="id"
        $this->validation->id = $id;

        // set user message
        $data['message'] = '<div class="success">Transaction Successfully Completed</div>';

        // load view
        $this->load->view('leaveform', $data);
    }

    function underConstruct() {
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $data['title'] = 'Under Consruct';
        $data['message'] = '';
        $data['action'] = site_url('person/underConstruct');
        $data['link_back'] = anchor('person/index/', 'Back to list of Nurses', array('class' => 'back'));
        //$this->load->helper('html');
        $this->load->view('uc1', $data);
    }

    function index($offset = 0) {
        // set validation properties
        $this->_set_fields();

        // set common properties
        $data['title'] = 'Nurses Login';

        $data['message'] = '';
        $data['action'] = site_url('person/login');


        $data['link_back'] = anchor('person/index/', 'Back to list of Nurses', array('class' => 'back'));
        $this->load->view('personLogin', $data);
    }

    function loginadmin($offset = 0) {

        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment);

        // load data
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HISWS/rest/registration/nursesTable');
        $persons = json_decode($_resultFromService, true);

        $this->load->library('pagination');
        $config['base_url'] = site_url('person/loginadmin/');
        $config['total_rows'] = $this->personModel->count_all();
        $config['per_page'] = 10;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $myimage = $this->getImage();
        $data['image'] = $myimage;
        // generate table data
        $this->load->library('table');
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('No', 'Name', 'Ward', 'Category', 'Actions');
        $i = 0 + $offset;
        foreach ($persons as $value) {
            $this->table->add_row(++$i, $value['name'], $value['ward'], strtoupper($value['category']) == 'M' ? 'Matron' : 'Nurse', anchor('person/view/' . $value['id'], 'view', array('class' => 'view')) . ' ' .
                    anchor('person/update/' . $value['id'], 'update', array('class' => 'update')) . ' ' .
                    anchor('person/delete/' . $value['id'], 'delete', array('class' => 'delete', 'onclick' => "return confirm('Are you sure want to delete this person?')")));
        }
        $data['table'] = $this->table->generate();


        // load view
        $this->load->view('personList', $data);
    }

    function loginnurse($offset = 0) {


        $this->_set_fieldsleave();
        $this->load->library('session');
        $uid = $this->session->userdata('username');
        //$person = $this->personModel->get_by_nic($uid)->row();
        $this->_set_fieldsview();
        $this->load->library('curl');

        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/registration/nursesLeaveForm/' . $uid
        );
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        foreach ($_resultsAfterDecode as $value) {
            $this->validation->name = $value['name'];
            $this->validation->ward = $value['ward'];
            $this->validation->grade = $value['grade'];
            break;
        }
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        // set common properties
        $data['title'] = 'Nurses Leaves';
        $data['message'] = '';
        $data['action'] = site_url('person/addLeaveNurse');
        $data['link_back'] = anchor('person/leaveHistory/', 'Back to Nurses Home Page', array('class' => 'back'));

        // load view
        $this->load->view('leavenurse', $data);
        ;
        $data['message'] = '<div class="success">Registered Successfully</div>';
        // generate table data
        $this->load->library('table');
    }

    function add() {
        // set validation properties
        $this->_set_fields();
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        // set common properties
        $data['title'] = 'Nurses Registration';
        $data['message'] = '';
        $data['wardList'] = $this->personModel->getWards();
        $data['gradeList'] = $this->personModel->getGrades();
        $data['action'] = site_url('person/addPerson');
        $data['link_back'] = anchor('person/loginadmin/', 'Back to list of Nurses', array('class' => 'back'));
        $data['no'] = "Ashan";
        // load view
        /* modified */
        $this->load->view('personRegister', $data);
    }

    function addPerson() {

        $_name = $this->input->post('name');
        $_address = $this->input->post('address');
        $_nic = $this->input->post('nic');
        $_ward = $this->input->post('ward');
        $_grade = $this->input->post('grade');
        $_category = $this->input->post('category');
        $_gender = $this->input->post('gender');
        $_dob = $this->input->post('dob');


        $_serviceUrl = "http://localhost:8080/HISWS/rest/registration/insert";


        $_curl = curl_init($_serviceUrl);

        $person = array('name' => $_name,
            'address' => $_address,
            'nic' => $_nic,
            'ward' => $_ward,
            'grade' => $_grade,
            'category' => $_category,
            'gender' => $_gender,
            'dob' => $_dob
        );

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
        // set common properties
        /* $data['title'] = 'Nurses Registration';
          $data['wardList'] = $this->personModel->getWards();
          $data['gradeList'] = $this->personModel->getGrades();
          $data['action'] = site_url('person/addPerson');
          $data['link_back'] = anchor('person/loginadmin/','Back to list of Nurses',array('class'=>'back'));
          $myimage=$this->getImage();
          $data['image']=$myimage;
          // set validation properties
          $this->_set_fields();
          $this->_set_rules();


          $this->form_validation->set_rules('name', 'Name ', 'required');
          $this->form_validation->set_rules('address', 'Address ', 'required');
          $this->form_validation->set_rules('nic', 'NIC ', 'trim|required|exact_length[10]|');
          $this->form_validation->set_rules('dob', 'Date of Birth ', 'trim|required|exact_length[10]');
          if ($this->form_validation->run() == FALSE)
          {	$data['message'] = '<div class="success">Registered Faild</div>';
          /*modified */
        /* 	$this->load->view('personRegister', $data);
          }else{
          $str=$this->input->post('nic');
          $result=$this->NicFormat($str);
          if($result==1){
          // save data
          $exitdata = $this->personModel->checkRecord($str);
          if($exitdata == -5){
          $data['message'] = '<div class="success">PREVIOUS RECORD WAS FOUND</div>';
          /*modified */
        /* 		$this->load->view('personRegister', $data);
          }
          else
          {
          $uname=$this->input->post('nic');
          $pword=$this->input->post('nic');
          $role=$this->input->post('category');


          $person = array('Name' => $this->input->post('name'),
          'Address' => $this->input->post('address'),
          'NIC' => $this->input->post('nic'),
          'Ward' => $this->input->post('ward'),
          'Gender' => $this->input->post('gender'),
          'Category' => $this->input->post('category'),
          'Ward' => $this->input->post('ward'),
          'Grade' => $this->input->post('grade'),
          'DOB' => date('Y-m-d', strtotime($this->input->post('dob'))));

          $person1 = array('UserName' => $uname,
          'Password' => $pword,
          'Category' =>  $role,
          );
          $data_ary = array(
          //'title'     => $upload_data['client_name'],
          'file'      => 'user.jpg',
          'width'     => 50,
          'height'    => 60,
          'type'      => 'jpeg',
          'size'      => 1,
          'NIC'		=> $uname,
          'date'      => time(),
          );

          $this->load->database();
          $this->db->insert('upload', $data_ary);
          $id = $this->personModel->save($person);
          $id1 = $this->personModel->saveLogin($person1);
          // set form input name="id"
          $this->validation->id = $id;

          // set user message
          $data['message'] = '<div class="success">Registered Successfully</div>';
          // load view
          /*modified */
        /* $this->load->view('personRegister', $data);
          }
          }
          else
          {
          $data['message'] = '<div class="success">Invalid NIC Format (EX : 123456789V) </div>';
          /*modified */
        /* $this->load->view('personRegister', $data);
          }
          } */
    }

    function login() {
        $uname = $this->input->post('uname');
        $pword = $this->input->post('pword');
        $this->load->library('session');

        $this->session->set_userdata('username', $uname);
        $this->form_validation->set_rules('uname', 'UserName ', 'trim|required');
        $this->form_validation->set_rules('pword', 'PassWord ', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['action'] = site_url('person/login');
            $data['message'] = "";
            $this->load->view('personLogin', $data);
        } else {


            $_serviceUrl = "http://localhost:8080/HISWS/rest/login/checkLogin";
            $_curl = curl_init($_serviceUrl);
            $person = array('username' => $uname,
                'password' => $pword
            );
            $_dataString = json_encode($person);
            curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
            curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                    $_curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($_dataString))
            );

            $_result = curl_exec($_curl);
            $id = (int) $_result;
            if ($id != -10) {
                //$id2 = $this->personModel->checkrole($uname);
                if ($id == 5) {
                    redirect('person/loginadmin/', 'refresh');
                } else {
                    redirect('person/leaveHistory/', 'refresh');
                }
            } else {
                $data['action'] = site_url('person/login');
                $data['message'] = 'Invalid Username Or Password';
                $this->load->view('personLogin', $data);
            }
        }
    }

    function view($id) {
        // set common properties
        $this->_set_fieldsview();
        $this->load->library('curl');

        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/registration/nursesView/' . $id
        );

        $_resultsAfterDecode = json_decode($_resultFromService, true);




        foreach ($_resultsAfterDecode as $value) {

            $this->validation->name = $value['name'];
            $this->validation->address = $value['address'];
            $this->validation->nic = $value['nic'];
            $this->validation->gender = $value['gender'];
            $this->validation->ward = $value['ward'];
            $this->validation->grade = $value['grade'];
            $this->validation->dob = $value['dob'];
            $this->validation->category = $value['category'];
            break;
        }

        $this->validation->id = $id;
        $data['title'] = 'Nurses Details';
        $data['link_back'] = anchor('person/loginadmin/', 'Back to list of Nurses', array('class' => 'back'));
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        // get person details
        // load view
        $this->load->view('personView', $data);
    }

    function update($id) {
        // set validation properties
        $this->_set_fields();

        // prefill form values
        /* $person = $this->personModel->get_by_id($id)->row();
          $this->validation->id = $id;
          $this->validation->name = $person->Name;
          $this->validation->address = $person->Address;
          $this->validation->nic = $person->NIC;
          //$_POST['nic'] = $person->NIC;
          $_POST['gender'] = strtoupper($person->Gender);
          $data['wardList'] = $this->personModel->getWards();
          $data['gradeList'] = $this->personModel->getGrades();
          $this->validation->dob = date('d-m-Y',strtotime($person->DOB)); */
        $this->load->library('curl');

        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/registration/nursesView/' . $id
        );

        $_resultsAfterDecode = json_decode($_resultFromService, true);


        foreach ($_resultsAfterDecode as $value) {

            $this->validation->name = $value['name'];
            $this->validation->address = $value['address'];
            $this->validation->nic = $value['nic'];
            $this->validation->gender = $value['gender'];
            $data['myward'] = $value['ward'];
            $data['mygrade'] = $value['grade'];
            $this->validation->dob = $value['dob'];
            $this->validation->category = $value['category'];
            break;
        }

        $this->validation->id = $id;

        // set common properties
        $data['title'] = 'Update Nurse';
        $data['message'] = '';
        $data['action'] = site_url('person/updatePerson');
        $data['link_back'] = anchor('person/loginadmin/', 'Back to list of Nurses', array('class' => 'back'));
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        // load view
        $this->load->view('personRegisterUpdate', $data);
    }

    function updateLeave($id) {
        // set validation properties
        $this->_set_fieldsleave();
        /* $person = $this->personModel->get_by_id_leave($id)->row(); */

        $this->_set_fieldsview();
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/leaves/leavesView/' . $id
        );
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        foreach ($_resultsAfterDecode as $person) {
            $this->validation->name = $person['name'];
            $this->validation->txtDate = $person['applyDate'];
            //$this->validation->ltype= $person->LeaveType;
            $this->validation->nleaves = $person['noLeaves'];
            $this->validation->rleaving = $person['reason'];
            $this->validation->dob = $person['leaveCommence'];
            $this->validation->grade = $person['grade'];
            $this->validation->ward = $person['ward'];
            $this->validation->ltype = $person['leaveType'];
            $this->validation->rresult = $person['result'];
            break;
        }
        $this->validation->id = $id;
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $data['title'] = 'Nurses Request Leaves';
        $data['message'] = '';
        $data['action'] = site_url('person/leaveAcceptance');
        $data['link_back'] = anchor('person/leavelist/', 'Back to Leave List', array('class' => 'back'));
        $this->load->view('leaveaccept', $data);
    }

    function updatePerson() {

        $_id = $this->input->post('id');
        $_name = $this->input->post('name');
        $_address = $this->input->post('address');
        $_nic = $this->input->post('nic');
        $_gender = $this->input->post('gender');
        $_category = $this->input->post('category');
        $_grade = $this->input->post('grade');
        $_ward = $this->input->post('ward');
        $_dob = $this->input->post('dob');



        $_serviceUrl = "http://localhost:8080/HISWS/rest/registration/update";


        $_curl = curl_init($_serviceUrl);

        $person = array('id' => $_id,
            'name' => $_name,
            'address' => $_address,
            'nic' => $_nic,
            'gender' => $_gender,
            'category' => $_category,
            'grade' => $_grade,
            'ward' => $_ward,
            'dob' => $_dob);

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
        $this->update($_id);
        // set common properties
        /* $data['title'] = 'Update Nurse';
          $data['action'] = site_url('person/updatePerson');
          $data['link_back'] = anchor('person/loginadmin/','Back to list of Nurses',array('class'=>'back'));
          $myimage=$this->getImage();
          $data['image']=$myimage;
          // set validation properties
          $this->_set_fields();
          $this->_set_rules();
          $data['wardList'] = $this->personModel->getWards();
          $data['gradeList'] = $this->personModel->getGrades();
          // save data
          $this->form_validation->set_rules('name', 'Name ', 'required');
          $this->form_validation->set_rules('address', 'Address ', 'required');
          $this->form_validation->set_rules('nic', 'NIC ', 'trim|required|exact_length[10]|');
          $this->form_validation->set_rules('dob', 'Date of Birth ', 'trim|required|exact_length[10]');
          if ($this->form_validation->run() == FALSE)
          {	$data['message'] = '<div class="success">Registered Faild</div>';
          $this->load->view('personEditUpdate', $data);
          }else
          {
          $str=$this->input->post('nic');
          $result=$this->NicFormat($str);
          if($result==1){
          $id = $this->input->post('id');
          $person = array('Name' => $this->input->post('name'),
          'Address' => $this->input->post('address'),
          'NIC' => $this->input->post('nic'),
          'Ward' => $this->input->post('ward'),
          'Gender' => $this->input->post('gender'),
          'Category' => $this->input->post('category'),
          'Ward' => $this->input->post('ward'),
          'Grade' => $this->input->post('grade'),
          'DOB' => date('Y-m-d', strtotime($this->input->post('dob'))));
          $this->personModel->update($id,$person);

          // set user message
          $data['message'] = '<div class="success">Update Nurse Success</div>';


          // load view
          $this->load->view('personEditUpdate', $data);
          }
          else{
          $data['message'] = '<div class="success">Invalid NIC Format (EX : 123456789V)</div>';
          $this->load->view('personEditUpdate', $data);
          }
          }
         * */
    }

    function delete($id) {
        // delete person
        $_serviceUrl = "http://localhost:8080/HISWS/rest/registration/delete";
        $_curl = curl_init($_serviceUrl);

        $person = array('id' => $id);

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
        redirect('person/loginadmin/', 'refresh');
    }

    function acceptLeave($id) {
        $_serviceUrl = "http://localhost:8080/HISWS/rest/leaves/acceptLeave";
        $_curl = curl_init($_serviceUrl);
        $person = array('id' => $id);
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
        // redirect to person list page
        redirect('person/leavelist/', 'refresh');
    }

    function rejectLeave($id) {
        // delete person
        $_serviceUrl = "http://localhost:8080/HISWS/rest/leaves/rejectLeave";
        $_curl = curl_init($_serviceUrl);
        $person = array('id' => $id);
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;

        // redirect to person list page
        redirect('person/leavelist/', 'refresh');
    }

    // validation fields
    function _set_fields() {
        $fields['id'] = 'id';
        $fields['name'] = 'name';
        $fields['address'] = 'address';
        $fields['gender'] = 'gender';
        $fields['nic'] = 'nic';
        $fields['dob'] = 'dob';

        $this->validation->set_fields($fields);
    }

    // validation fields
    function _set_fieldsleave() {
        $fields['id'] = 'id';
        $fields['name'] = 'name';
        $fields['txtDate'] = 'txtDate';
        $fields['ltype'] = 'ltype';
        $fields['nleaves'] = 'nleaves';
        $fields['grade'] = 'grade';
        $fields['ward'] = 'ward';
        $fields['rleaving'] = 'rleaving';
        $fields['dob'] = 'dob';
        $fields['rresult'] = 'rresult';
        $this->validation->set_fields($fields);
    }

    function _set_fieldsview() {
        $fields['id'] = 'id';
        $fields['name'] = 'name';
        $fields['address'] = 'address';
        $fields['nic'] = 'nic';
        $fields['category'] = 'category';
        $fields['gender'] = 'gender';
        $fields['grade'] = 'grade';
        $fields['ward'] = 'ward';
        $fields['dob'] = 'dob';
        $this->validation->set_fields($fields);
    }

    // validation rules
    function _set_rules() {
        $rules['name'] = 'trim|required';
        $rules['gender'] = 'trim|required';
        $rules['address'] = 'trim|required';
        $rules['nic'] = 'trim|required';
        $rules['dob'] = 'trim|required|callback_valid_date';
        $rules['nleaves'] = 'trim|required';

        $this->validation->set_rules($rules);

        $this->validation->set_message('required', '* required');
        $this->validation->set_message('isset', '* required');
        $this->validation->set_error_delimiters('<p class="error">', '</p>');
    }

    // date_validation callback
    function valid_date($str) {
        if (!ereg("^(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-([0-9]{4})$", $str)) {
            $this->validation->set_message('valid_date', 'date format is not valid. dd-mm-yyyy');
            return false;
        } else {
            return true;
        }
    }

    function valid_nic($str) {
        if (!ereg("^([0-9][0-9]0-9][0-9]0-9][0-9]0-9][0-9]V)$", $str)) {
            $this->validation->set_message('valid Nic', 'NIC format is:123456789V');
            return false;
        } else {
            return true;
        }
    }

    function getImage() {
        $this->load->library('session');
        $uid = $this->session->userdata('username');
        $path;
        /* $this->load->model('personModel','',TRUE);
          $persons = $this->personModel->get_by_nic_image($uid)->result();
          foreach ($persons as $person){
          $path=$person->file;


          }

         */
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/profile/getProfileImage/' . $uid
        );
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        foreach ($_resultsAfterDecode as $mypath) {
            $path = $mypath[1];
        }
        $path = 'uploads/' . $path;
        return $path;
    }

    function reset_admin() {
        // set validation properties
        $this->_set_fields();
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        // set common properties
        $data['title'] = 'Profile Settings';
        $data['message'] = 'Fill All Feilds';
        $data['action'] = site_url('person/reset_password_admin');
        $data['link'] = "person/loginadmin/";

        // load view
        $this->load->view('resetpassword', $data);
    }

    function reset_password_admin() {

        $pword = $this->input->post('pword');
        $cpword = $this->input->post('cpword');
        $this->load->library('session');

        $uid = $this->session->userdata('username');
        $this->form_validation->set_rules('pword', 'Password', 'trim|required');
        $this->form_validation->set_rules('cpword', 'Password Confirmation', 'trim|required|matches[pword]');
        if ($this->form_validation->run() == FALSE) {
            $data['action'] = site_url('person/reset_password_admin');
            $myimage = $this->getImage();
            $data['image'] = $myimage;
            $data['message'] = "Fail to Reset Password";
            $data['link'] = "person/loginadmin/";
            $this->load->view('resetpassword', $data);
        } else {
            $data = array(
                'Password' => $pword,
                'username' => $uid
            );
            $_serviceUrl = "http://localhost:8080/HISWS/rest/profile/resetPassword";
            $_curl = curl_init($_serviceUrl);
            $_dataString = json_encode($data);

            curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
            curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                    $_curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($_dataString))
            );

            $_result = curl_exec($_curl);


            $myimage = $this->getImage();
            $data['image'] = $myimage;
            $data['action'] = site_url('person/reset_password_admin');
            $data['message'] = "Your Password " . $_result;
            $data['link'] = "person/loginadmin/";
            $this->load->view('resetpassword', $data);
        }
    }

    function reset_nurse() {
        // set validation properties
        $this->_set_fields();
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        // set common properties
        $data['title'] = 'Profile Settings';
        $data['message'] = 'Fill All Feilds';
        $data['action'] = site_url('person/reset_password_nurse');
        $data['link'] = "person/leaveHistory/";

        // load view
        $this->load->view('resetpassword', $data);
    }

    function reset_password_nurse() {

        $pword = $this->input->post('pword');
        $pcpword = $this->input->post('cpword');
        $this->load->library('session');
        $uid = $this->session->userdata('username');
        $this->form_validation->set_rules('pword', 'Password', 'trim|required');
        $this->form_validation->set_rules('cpword', 'Password Confirmation', 'trim|required|matches[pword]');
        if ($this->form_validation->run() == FALSE) {
            $data['action'] = site_url('person/reset_password_nurse');
            $myimage = $this->getImage();
            $data['image'] = $myimage;
            $data['message'] = "Fail to Reset Password";
            $data['link'] = "person/leaveHistory/";
            $this->load->view('resetpassword', $data);
        } else {
            $data = array(
                'Password' => $pword,
                'username' => $uid
            );
            $_serviceUrl = "http://localhost:8080/HISWS/rest/profile/resetPassword";
            $_curl = curl_init($_serviceUrl);
            $_dataString = json_encode($data);

            curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
            curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                    $_curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($_dataString))
            );

            $_result = curl_exec($_curl);
            $myimage = $this->getImage();
            $data['image'] = $myimage;
            $data['action'] = site_url('person/reset_password_nurse');
            $data['message'] = "Your Password " . $_result;
            $data['link'] = "person/leaveHistory/";
            $this->load->view('resetpassword', $data);
        }
    }

    function NicFormat($str) {
        $ans = 1;
        $digits = 0;
        $i = 0;
        $pos = strpos($str, "V");
        if ($pos != 9) {
            $ans = -1;
        }
        while ($i < strlen($str)) {
            if (preg_match("/[0-9]/", $str{$i})) {
                $digits++;
            }
            ++$i;
        }
        if ($digits != 9) {
            $ans = -1;
        }
        return $ans;
    }

    function getWardList() {

        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HISWS/rest/registration/getWards');
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        echo json_encode($_resultsAfterDecode);
    }

    function getGradeList() {

        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HISWS/rest/grade/getGrades');
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        echo json_encode($_resultsAfterDecode);
    }

}

?>