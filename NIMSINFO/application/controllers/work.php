<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Work extends CI_Controller {

    public function index() {
        
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->view('work',$data);
    }
    
     function getImage() {
        $this->load->library('session');
        $uid = $this->session->userdata('username');
        $path = "";
        
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/profile/getProfileImage/' . $uid
        );
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        foreach ($_resultsAfterDecode as $mypath) {
            $path = $mypath[1];
        }
        $path = 'uploads/' . $path;
        return $path;
    }
    
    
    public function add() {
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->view('work',$data);
    }

    public function update() {
        $myimage = $this->getImage();
        $data['image'] = $myimage;
        $this->load->view('workupdate',$data);
    }
    
   public function getWards() {
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HIS/rest/registration/getWards');
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        echo json_encode($_resultsAfterDecode);
    }

    public function getNurseDetailsByWard($_category) {

        $this->load->library('curl');
        $_catAfterStrReplace = str_replace(' ', '%20', $_category);
        $_resultFromService = $this->curl->simple_get(
                'http://localhost:8080/HISWS/rest/registration/getNursesByWard/' . $_catAfterStrReplace
        );

        $_resultsAfterDecode = json_decode($_resultFromService);
        echo json_encode($_resultsAfterDecode);
    }

    public function getSchedules() {
        $this->load->library('curl');
        $_resultFromService = $this->curl->simple_get('http://localhost:8080/HIS/rest/schedule/getSchedules');
        $_resultsAfterDecode = json_decode($_resultFromService, true);
        echo json_encode($_resultsAfterDecode);
    }

    public function addSchedule()
       {

        $_week = $this->input->post('week');
        $_nic = $this->input->post('nic');
        $_year = $this->input->post('year');
        $_month = $this->input->post('month');
        $_ward = $this->input->post('ward');
        $_monday = $this->input->post('monday');
        $_tuesday = $this->input->post('tuesday');
        $_wednesday = $this->input->post('wednesday');
        $_thursday = $this->input->post('thursday');
        $_friday = $this->input->post('friday');
        $_saturday = $this->input->post('saturday');
        $_sunday = $this->input->post('sunday');

  
        $_serviceUrl = "http://localhost:8080/HISWS/rest/work/insert";

        $_curl = curl_init($_serviceUrl);

        $person = array(
            'nic' => $_nic,
            'year' => $_year,
            'month' => $_month,
            'ward' => $_ward,
            'week' => $_week,
            'monday' => $_monday,
            'tuesday' => $_tuesday,
            'wednesday' => $_wednesday,
            'thursday' => $_thursday,
            'friday' => $_friday,
            'saturday' => $_saturday,
            'sunday' => $_sunday);
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
    
        
       }

    public function getNursesSchedule()
    {
        $_week = $this->input->post('week');
        $_year = $this->input->post('year');
        $_month = $this->input->post('month');
        $_ward = $this->input->post('ward');


        $_serviceUrl = "http://localhost:8080/HISWS/rest/work/nurseSchedules";


        $_curl = curl_init($_serviceUrl);

        $person = array('year' => $_year,
                        'month' => $_month,
                        'ward' => $_ward,
                        'week' => $_week);

        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        $_resultsAfterDecode = json_decode( $_result);
        echo json_encode($_resultsAfterDecode);
    
        
    }

        public function updateSchedule() {
        
        $_id  = $this->input->post('id');
        $_monday = $this->input->post('monday');
        $_tuesday = $this->input->post('tuesday');
        $_wednesday = $this->input->post('wednesday');
        $_thursday = $this->input->post('thursday');
        $_friday = $this->input->post('friday');
        $_saturday = $this->input->post('saturday');
        $_sunday = $this->input->post('sunday');


        $_serviceUrl = "http://localhost:8080/HISWS/rest/work/update";


        $_curl = curl_init($_serviceUrl);

        $person = array('id' => $_id, 
            'monday' => $_monday,
            'tuesday' => $_tuesday,
            'wednesday' => $_wednesday,
            'thursday' => $_thursday,
            'friday' => $_friday,
            'saturday' => $_saturday,
            'sunday' => $_sunday);
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
    }
    
        public function deleteSchedule() {
        
        $_id  = $this->input->post('id');
        $_name = $this->input->post('name');
        $_week = $this->input->post('week');
        $_nic = $this->input->post('nic');
        $_year = $this->input->post('year');
        $_month = $this->input->post('month');
        $_ward = $this->input->post('ward');
        $_monday = $this->input->post('monday');
        $_tuesday = $this->input->post('tuesday');
        $_wednesday = $this->input->post('wednesday');
        $_thursday = $this->input->post('thursday');
        $_friday = $this->input->post('friday');
        $_saturday = $this->input->post('saturday');
        $_sunday = $this->input->post('sunday');


        $_serviceUrl = "http://localhost:8080/HIS/rest/work/delete";


        $_curl = curl_init($_serviceUrl);

        $person = array('id' => $_id, 
            'name' => $_name,
            'nic' => $_nic,
            'year' => $_year,
            'month' => $_month,
            'ward' => $_ward,
            'week' => $_week,
            'monday' => $_monday,
            'tuesday' => $_tuesday,
            'wednesday' => $_wednesday,
            'thursday' => $_thursday,
            'friday' => $_friday,
            'saturday' => $_saturday,
            'sunday' => $_sunday);
        $_dataString = json_encode($person);

        curl_setopt($_curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($_curl, CURLOPT_POSTFIELDS, $_dataString);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                $_curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_dataString))
        );

        $_result = curl_exec($_curl);
        echo $_result;
    }
    
    
}

?>
