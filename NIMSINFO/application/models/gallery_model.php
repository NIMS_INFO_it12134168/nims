<?php
class Gallery_model extends Model {
 var $gallery_path;
	function Gallery_model()
	{
		parent::Model();
		$this->gallery_path=realpath(APPPATH .'../images');
	}
	function do_upload()
	{
	 $config = array(
		'allowed_types' =>'jpg|jpeg|png|gif',
		'upload_path' =>$this->gallery_path
	 );
	 $this->load->library('upload',$config);
	 $this->upload->do_upload();
	}
}
?>