<?php
class OvertimeModel extends CI_Model {
	
	private $tbl_overtime = 'overtimenew';
	
	private $tbl_overtimeview = 'overtimeview';
	
	private $tbl_registration = 'overtimeregistration';
	
	private $tbl_register = 'registration';
	
	private $tbl_gradeandbasicsalary = 'gradeandbasicsalary';
	
	function Overtime(){
		parent::__construct();
	}
	
	function list_all(){
		$this->db->order_by('id','asc');
		return $this->db->get($tbl_overtime);
	}
	
	function count_all(){
		return $this->db->count_all($this->tbl_overtime);
	}
	
	function get_paged_list($limit = 10, $offset = 0){
		$this->db->order_by('id','asc');
		return $this->db->get($this->tbl_overtime, $limit, $offset);
	}
	
	//count all overtimeview
	function count_all_overtimeview(){
		return $this->db->count_all($this->tbl_overtimeview);
	}
	
	//overtimeview get_paged_list
	function get_paged_list_overtimeview($limit = 10,$offset = 0){
		//$this->db->order_by('id','asc');
		$this->db->select('nic');
		$this->db->select('name');
		$this->db->select('month');
		$this->db->select('year');
		$this->db->select('ward');
		$this->db->select_sum('hours');
		$this->db->select_sum('pholiday');
		$this->db->select_sum('offday');
		
		
		$this->db->group_by(array("nic", "month" ,"year" ));
		return $this->db->get($this->tbl_overtime,$limit,$offset);
			
	}
	function get_paged_list_overtimeviewbywardyearmonth($ward,$year,$month,$limit = 10, $offset = 0)
	{
		$this->db->select('nic');
		$this->db->select('name');
		$this->db->select('month');
		$this->db->select('year');
		$this->db->select_sum('hours');
		$this->db->select_sum('pholiday');
		$this->db->select_sum('offday');
		$this->db->where('ward', $ward);
		$this->db->where('year', $year);
		$this->db->where('month', $month);
		$this->db->group_by(array("nic","ward","month","year" ));
		return $this->db->get($this->tbl_overtime,$limit,$offset);
		
	}
	
	function selectbasicsalary($grade)
	{	
		$this->db->select('basicsalary');
		$this->db->where('grade',$grade);
		return $this->db->get($this->tbl_gradeandbasicsalary);		
	}
	
	function get_by_id($id){
		$this->db->where('id', $id);
		return $this->db->get($this->tbl_overtime);
	}
	
	//get data from the overtimeview
	function get_by_id_overtimeview($id,$month){
		$this->db->where('id',$id);
		$this->db->where('month',$month);
		return $this->db->get($this->tbl_overtimeview);
	}
	
	//get data from the registration table
	function get_by_id_registration($id){
		$this->db->where('id',$id);
		return $this->db->get($this->tbl_registration);
		
	}
	
	
	
	function save($overtime){
		if($this->db->insert($this->tbl_overtime, $overtime)){
		return true;
		}
		else
		return false;
	}
	
	function update($id, $overtime){
		$this->db->where('id', $id);
		$this->db->update($this->tbl_overtime, $overtime);
	}
	
	function delete($pk){
		$this->db->where('pk', $pk);
		$this->db->delete($this->tbl_overtime);
	}
	
	
	
	//new functions added
	function registerednurses(){
		//$query = $this->db->query('SELECT Name FROM registration');
       // return $query->result();
        $this->db->select('name');
        $this->db->from('registration');
        $q = $this->db->get('');
        if($q->num_rows() > 0) 
        {
            $data = array();
            foreach($q->result() as $row) 
            {
                $data=$row;
            }
            return $data;
        }
	}

	function getWards() {
		$data = array();
		$Q = $this->db->get('ward');
		if ($Q->num_rows() > 0) {
			foreach ($Q->result_array() as $row){
		         	$data[] = $row;
		        }
		}	
		$Q->free_result();
		return $data;	
	}
	
	function get_paged_Name($nic)
	{
		
		$this->db->select('Name,NIC,Ward');
        $this->db->from('registration');
        $this->db->where('NIC', $nic);
        $this->db->limit(1);
		return $this->db->get();
		
	}
}
?>