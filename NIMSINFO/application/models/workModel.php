<?php
class WorkModel extends CI_Model {
	
	private $tbl_person= 'tbl_person';
	private $tbl_person1= 'registration';
	function Work(){
		parent::__construct();
	}
	
	function list_all(){
		$this->db->order_by('id','asc');
		return $this->db->get($tbl_person);
	}
	
	function count_all(){
		return $this->db->count_all($this->tbl_person);
	}
	
	function get_paged_list($limit = 10, $offset = 0){
		//$this->db->order_by('id','asc');
		return $this->db->get($this->tbl_person, $limit, $offset);
	}
	
	function get_paged_list_selectnurseswardmonthweek($ward,$month,$weekname,$limit = 10, $offset = 0)
	{
		$this->db->where('ward',$ward);
		$this->db->where('month',$month);
		$this->db->where('weekname',$weekname);
		return $this->db->get($this->tbl_person);
	}
	function get_paged_Name($nic)
	{
		
		$this->db->select('Name,NIC,Ward');
        $this->db->from('registration');
        $this->db->where('NIC', $nic);
        $this->db->limit(1);
		return $this->db->get();
		
	}
	
	
	function get_by_id($id){
		$this->db->where('id', $id);
		return $this->db->get($this->tbl_person);
	}
	
	function save($person){
		if($this->db->insert($this->tbl_person, $person)){
		return true;
		}
		else{
		return false;
		}
		
	}
	
	function update($id, $person){
		$this->db->where('id', $id);
		$this->db->update($this->tbl_person, $person);
	}
	
	function delete($name,$ward,$month,$weekname){
		$this->db->where('name', $name);
		$this->db->where('ward', $ward);
		$this->db->where('month', $month);
		$this->db->where('weekname', $weekname);
		$this->db->delete($this->tbl_person);
	}
	function getWards() {
		$data = array();
		$Q = $this->db->get('ward');
		if ($Q->num_rows() > 0) {
			foreach ($Q->result_array() as $row){
		         	$data[] = $row;
		        }
		}	
		$Q->free_result();
		return $data;	
	}
	function get_AllSchedule($limit = 10, $offset = 0,$id){
		//$this->db->order_by('ApplyDate','desc');
		return $this->db->get_where($this->tbl_person, array('NIC' => $id,'year'=>date('Y')), $limit, $offset);
	}
	function get_AllScheduleByMonth($limit = 10, $offset = 0,$id,$month){
		//$this->db->order_by('ApplyDate','desc');
		return $this->db->get_where($this->tbl_person, array('NIC' => $id,'year'=>date('Y'),'month'=>$month), $limit, $offset);
	}
	function count_all_work_personschedule($id){
		$this->db->where('NIC', $id);
		$this->db->where('year', date('Y'));
		return $this->db->count_all_results($this->tbl_person);
	}
	function count_all_work_personscheduleBymonth($id,$month){
		$this->db->where('NIC', $id);
		$this->db->where('month', $month);
		$this->db->where('year', date('Y'));
		return $this->db->count_all_results($this->tbl_person);
	}
	function get_by_userid($id){
		$this->db->where('id', $id);
		return $this->db->get($this->tbl_person);
	}	
}
?>