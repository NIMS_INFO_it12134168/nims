<?php
class PersonModel extends CI_Model {
	
	private $tbl_person= 'registration';
	private $tbl_person1= 'leaves';
	private $tbl_person2= 'login';
	private $tbl_person3= 'upload';
	function Person(){
		parent::_consruct();
	}
	
	function list_all(){
		$this->db->order_by('id','asc');
		return $this->db->get($tbl_person);
	}
	function list_all_leave(){
		$this->db->order_by('ApplyDate','desc');
		return $this->db->get($this->tbl_person1);
	}
	
	function count_all(){
		return $this->db->count_all($this->tbl_person);
	}
	function count_all_leave_person($id){
		$this->db->where('NIC', $id);
		return $this->db->count_all_results($this->tbl_person1);
	}
	function count_all_leave(){
		
		return $this->db->count_all($this->tbl_person1);
	}
	function get_paged_list($limit = 10, $offset = 0){
		$this->db->order_by('id','asc');
		return $this->db->get($this->tbl_person, $limit, $offset);
	}
	function get_paged_list_leave($limit = 10, $offset = 0){
		//$start_date=date('d-m-Y', strtotime('-3 month'));
		//$this->db->where('ApplyDate  >=', $start_date);
		$this->db->order_by('LeaveCommence','asc');		 
		return $this->db->get($this->tbl_person1, $limit, $offset);
	}
	function get_paged_list_leave_group($limit = 10, $offset = 0,$id){
		
		//$this->db->order_by('ApplyDate','desc');
		return $this->db->order_by('ApplyDate','desc')->get_where($this->tbl_person1, array('NIC' => $id), $limit, $offset);
	
		
	}
	function get_by_id($id){
		$this->db->where('id', $id);
		return $this->db->get($this->tbl_person);
	}
	function get_by_nic($id){
		$this->db->where('NIC', $id);
		return $this->db->get($this->tbl_person);
	}

	function get_by_nic_image($id){
		$this->db->where('NIC', $id);
		return $this->db->get($this->tbl_person3);
	}
	function get_by_id_leave($id){
		$this->db->where('id', $id);
		return $this->db->get($this->tbl_person1);
	}
	function get_by_id_leaveRequset($id){
		$this->db->where('NIC', $id);
		return $this->db->get($this->tbl_person1);
	}
	
	function save($person){
		$this->db->insert($this->tbl_person, $person);
		return $this->db->insert_id();
	}
	function save1($person){
		$this->db->insert('leaves', $person);
		return $this->db->insert_id();
	}
	function check($username,$pass){
		 //create query to connect user login database
        $this->db->select('id, UserName, Password');
        $this->db->from('login');
        $this->db->where('UserName', $username);
        $this->db->where('Password',$pass);
        $this->db->limit(1);
		$query = $this->db->get();
        if($query->num_rows() == 1) { 
            return 10; //if data is true
        } else {
            return -5; //if data is wrong
        }
	}
		
	function checkrole($username){
		 //create query to connect user login database
		 $cat='M';
        $this->db->select('id, UserName, Password');
        $this->db->from('login');
        $this->db->where('UserName', $username);
        $this->db->where('Category',$cat);
        $this->db->limit(1);
		$query = $this->db->get();
        if($query->num_rows() == 1) { 
            return 10; //if data is true
        } else {
            return -5; //if data is wrong
        }
		
	}
	function saveLogin($person){
		$this->db->insert($this->tbl_person2, $person);
		return $this->db->insert_id();
	}
	function update($id, $person){
		$this->db->where('id', $id);
		$this->db->update($this->tbl_person, $person);
	}
	function updateleave($id,$person){
		$this->db->where('id', $id);
		$this->db->update($this->tbl_person1, $person);
	}
	
	function delete($id){
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_person);
	}
	function checkRecord($nic){
		 //create query to connect user login database
        $this->db->select('id');
        $this->db->from('registration');
        $this->db->where('NIC', $nic);
        $this->db->limit(1);
		$query = $this->db->get();
        if($query->num_rows() == 1) { 
            return -5; //if data is true
        } else {
            return 5; //if data is wrong
        }
	}
	/* ******Data Retrive for Dropbox***********/
	
	function getnursesbyward($ward,$limit = 10, $offset = 0){
		$this->db->where('Ward',$ward);
		return $this->db->get($this->tbl_person,$limit,$offset);
	}
 
	function getWards() {
		$data = array();
		$Q = $this->db->get('ward');
		if ($Q->num_rows() > 0) {
			foreach ($Q->result_array() as $row){
		         	$data[] = $row;
		        }
		}	
		$Q->free_result();
		return $data;	
	}
	function getGrades() {
		$data = array();
		$Q = $this->db->get('grade');
		if ($Q->num_rows() > 0) {
			foreach ($Q->result_array() as $row){
		         	$data[] = $row;
		        }
		}	
		$Q->free_result();
		return $data;	
	}
	function selectgrade($nic){
		$this->db->where('nic',$nic);
		return $this->db->get($this->tbl_person);
		
	}
	
	
}
?>