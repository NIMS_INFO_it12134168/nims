
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>NURSES SETTINGS</title>

<link href="<?php echo base_url(); ?>style/style.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="<?=base_url()?>assets/img/favicon.ico" type="image/ico">
<link href="<?php echo base_url(); ?>style/style1.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/main-style.css" rel="stylesheet" type="text/css" />


</head>

<body class="body-Login-back">

    <div class="container">
       
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center logo-margin ">
			<img src="<?php echo base_url(); ?><?php echo $image; ?>" height="150" width="150"/>
         
                </div>
				
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                  
                    <div class="panel-heading">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
					
                       <form  method="post" action="<?php echo $action; ?>">
                            <fieldset>
                               <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="pword" class="text" 
									type="password" autofocus  value = "<?php echo set_value('pword'); ?>" >
									<h5><span style="color:red;"><?php echo form_error('pword'); ?></h5>
								</div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Comfirm Password" name="cpword" type="password" value="<?php echo set_value('cpword'); ?>" class="text">
									<h5><span style="color:red;"><?php echo form_error('cpword'); ?></h5>
							   </div>
                                <div class="checkbox">
                                    
									<h5><span style="color:red;"><?php echo $message; ?></h5>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
			
		
                                <input type="submit" value="Reset" class="btn btn-lg btn-success btn-block">
								<a href="<?php echo site_url($link) ?>"><i class="fa fa-sign-out fa-fw"></i>Back</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/metisMenu/jquery.metisMenu.js"></script>
	<h5><?php echo form_error('pword'); ?></h5>
</body>
</html>