 <!-- navbar side -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar-collapse -->
            <div class="sidebar-collapse">
                <!-- side-menu -->
                <ul class="nav" id="side-menu">
                   <li>
                        <div class="user-section">
                            <div class="user-section-inner">
                               <img src="<?php echo base_url(); ?><?php echo $image; ?>" />
                            </div>
                            <div class="user-info">                                                           
                                <div class="user-text-online">
								<script language="JavaScript">
	if (document.all||document.getElementById)
	document.write('<span id="worldclock" style="font:bold 25px Arial;"></span><br />')

	zone=0;
	isitlocal=true;
	ampm='';	

	function updateclock(z){
	zone=z.options[z.selectedIndex].value;
	isitlocal=(z.options[0].selected)?true:false;
	}


	function WorldClock(){
	now=new Date();
	ofst=now.getTimezoneOffset()/60;
	secs=now.getSeconds();
	sec=-1.57+Math.PI*secs/30;
	mins=now.getMinutes();
	min=-1.57+Math.PI*mins/30;
	hr=(isitlocal)?now.getHours():(now.getHours() + parseInt(ofst)) + parseInt(zone);
	hrs=-1.575+Math.PI*hr/6+Math.PI*parseInt(now.getMinutes())/360;
	if (hr < 0) hr+=24;
	if (hr > 23) hr-=24;
	ampm = (hr > 11)?"PM":"AM";
	statusampm = ampm.toUpperCase();

	hr2 = hr;
	if (hr2 == 0) hr2=12;
	(hr2 < 13)?hr2:hr2 %= 12;
	if (hr2<10) hr2="0"+hr2

	var finaltime=hr2+':'+((mins < 10)?"0"+mins:mins)+':'+((secs < 10)?"0"+secs:secs)+' '+statusampm;

	if (document.all)
		worldclock.innerHTML=finaltime
	else if (document.getElementById)
	document.getElementById("worldclock").innerHTML=finaltime
	else if (document.layers){
	document.worldclockns.document.worldclockns2.document.write(finaltime)
	document.worldclockns.document.worldclockns2.document.close()
		}


	setTimeout('WorldClock()',1000);
	}

	window.onload=WorldClock
</script>

                                </div>
                            </div>
                        </div>
						</li>
                        <!--end user image section-->
                    </li>
					 <li class="">
                        <a href="<?php echo site_url('person/loginadmin/')?>"><i class="fa fa-dashboard fa-fw"></i>Home</a>
                    </li>
                   <li class="">
                        <a href="<?php echo site_url('person/add/')?>"><i class="fa fa-dashboard fa-fw"></i>Register New</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Work Shedule<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('work/')?>">Shedule New</a>
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                     <li>
                        <a href="<?php echo site_url('person/leavelist/')?>"><i class="fa fa-flask fa-fw"></i>Leave Request</a>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>OverTime<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('overtime/index')?>">New Overtime</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('overtime/overtimesummary')?>">Overtime Summary</a>
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                    
                    <li>
						<a href="<?php echo site_url('person/index/') ?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        
                    </li>
                    
                    <li>
                    
                    </li>
                    <li>
                <!-- end side-menu -->
            </div>
           
        </nav>