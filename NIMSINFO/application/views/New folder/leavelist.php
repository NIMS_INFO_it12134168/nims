<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>LEAVE REQUEST</title>

<link href="<?php echo base_url(); ?>style/style.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>style/style1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>script/calendar.js"></script>
<script language="javascript" 
type="text/javascript"> 
function showDate() { 
document 
.getElementById("dob") 
.value = dd(); 
} 
function dd(){
 var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = dd+'-'+mm+'-'+yyyy;
return today;
}
</script> 
</head>
<body onLoad="showDate()">
	 <div id="sidebar">
    <h2>QUICK LINKS</h2>
    <ul class="sidemenu">				
      <li id="leaverequest"> <?php echo anchor('person/leave/','Leave History',array('class'=>'add')); ?></li>
      <li id="workshedule"> <?php echo anchor('person/add/','Leave Acceptance',array('class'=>'add')); ?></li>
      <li id="OTsummary"> <?php echo anchor('person/add/','OT Summary',array('class'=>'add')); ?></li>
    </ul>	
</div>
	<div class="content">
		<?php echo $message; ?>
		<form>
		<div class="data">
		<table>
		<tr>
			<td valign="top">LEAVE APPLIED<span style="color:red;"></span></td>
			<td><input type="text" name="dob" onclick="displayDatePicker('dob');" class="text" value="<?php echo $this->validation->dob; ?>"/>
			<a href="javascript:void(0);" onclick="displayDatePicker('dob');"><img src="<?php echo base_url(); ?>style/images/calendar.png" alt="calendar" border="0"></a>
			<?php echo $this->validation->dob_error; ?></td>
		</tr>
		</table>
		</div>
		<div class="content">
		<h1>REQUESTED LEAVES</h1>
		<div class="paging"><?php echo $pagination; ?></div>
		<div class="data"><?php echo $table; ?></div>
		<br />
		
	</div>
		</form>
		<br /><?php echo $link_back; ?>
	</div>
</body>
</html>