<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>NURSES LEAVES</title>


        <script type="text/javascript" src="<?php echo base_url(); ?>Script/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>Script/overtime.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>Script/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>Script/common.js"></script>

        <link href="<?php echo base_url(); ?>Script/style.css" rel="stylesheet" type="text/css" />


        <link href="<?php echo base_url(); ?>datepicker/datepicker/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url(); ?>datepicker/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">



        <link rel="icon" href="<?= base_url() ?>assets/img/favicon.ico" type="image/ico">
        <link href="<?php echo base_url(); ?>style/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>style/style1.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/main-style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>style/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>style/style1.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>style/style2.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>style/style.css" rel="stylesheet" type="text/css" />


        <script type="text/javascript" src="<?php echo base_url(); ?>script/calendar.js"></script>

        <script language="javascript" type="text/javascript">
            function showDate() {
                document
                        .getElementById("dob")
                        .value = dd();
            }
            function dd() {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = dd + '-' + mm + '-' + yyyy;
                return today;
            }
        </script>

    </head>

    <body >
        <!--  wrapper -->
        <div id="wrapper">
            <!-- navbar top -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
                <!-- navbar-header -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>



                    <img src="<?php echo base_url(); ?>assets/img/hislogo.png" />         

                </div>
                <!-- end navbar-header -->
                <!-- navbar-top-links -->
                <ul class="nav navbar-top-links navbar-right">
                    <!-- main dropdown -->


                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-3x"></i>
                        </a>

                        <!-- dropdown user-->
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?php echo site_url('upload/do_upload/') ?>"><i class="fa fa-user fa-fw"></i>User Profile</a>
                            </li>
                            <li><a href="<?php echo site_url('person/reset_admin/') ?>"><i class="fa fa-gear fa-fw"></i>Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo site_url('person/index/') ?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a>

                            </li>
                        </ul>
                        <!-- end dropdown-user -->
                    </li>
                    <!-- end main dropdown -->
                </ul>
                <!-- end navbar-top-links -->

            </nav>
            <!-- end navbar top -->

            <!-- navbar side -->
            <nav class="navbar-default navbar-static-side" role="navigation">
                <!-- sidebar-collapse -->
                <div class="sidebar-collapse">
                    <!-- side-menu -->
                    <ul class="nav" id="side-menu">
                        <li>
                            <div class="user-section">
                                <div class="user-section-inner">
                                    <img src="<?php echo base_url(); ?><?php echo $image; ?>" />  
                                </div>
                                <div class="user-info">                                                           
                                    <div class="user-text-online">
                                        <script language="JavaScript">
                                            if (document.all || document.getElementById)
                                                document.write('<span id="worldclock" style="font:bold 25px Arial;"></span><br />')

                                            zone = 0;
                                            isitlocal = true;
                                            ampm = '';

                                            function updateclock(z) {
                                                zone = z.options[z.selectedIndex].value;
                                                isitlocal = (z.options[0].selected) ? true : false;
                                            }


                                            function WorldClock() {
                                                now = new Date();
                                                ofst = now.getTimezoneOffset() / 60;
                                                secs = now.getSeconds();
                                                sec = -1.57 + Math.PI * secs / 30;
                                                mins = now.getMinutes();
                                                min = -1.57 + Math.PI * mins / 30;
                                                hr = (isitlocal) ? now.getHours() : (now.getHours() + parseInt(ofst)) + parseInt(zone);
                                                hrs = -1.575 + Math.PI * hr / 6 + Math.PI * parseInt(now.getMinutes()) / 360;
                                                if (hr < 0)
                                                    hr += 24;
                                                if (hr > 23)
                                                    hr -= 24;
                                                ampm = (hr > 11) ? "PM" : "AM";
                                                statusampm = ampm.toUpperCase();

                                                hr2 = hr;
                                                if (hr2 == 0)
                                                    hr2 = 12;
                                                (hr2 < 13) ? hr2 : hr2 %= 12;
                                                if (hr2 < 10)
                                                    hr2 = "0" + hr2

                                                var finaltime = hr2 + ':' + ((mins < 10) ? "0" + mins : mins) + ':' + ((secs < 10) ? "0" + secs : secs) + ' ' + statusampm;

                                                if (document.all)
                                                    worldclock.innerHTML = finaltime
                                                else if (document.getElementById)
                                                    document.getElementById("worldclock").innerHTML = finaltime
                                                else if (document.layers) {
                                                    document.worldclockns.document.worldclockns2.document.write(finaltime)
                                                    document.worldclockns.document.worldclockns2.document.close()
                                                }


                                                setTimeout('WorldClock()', 1000);
                                            }

                                            window.onload = WorldClock
                                        </script>

                                    </div>
                                </div>
                            </div>
                        </li>
                        <!--end user image section-->
                        </li>
                        <li class="">
                            <a href="<?php echo site_url('person/loginadmin/') ?>"><i class="fa fa-dashboard fa-fw"></i>Home</a>
                        </li>
                     <li class="">
                        <a href="<?php echo site_url('person/add/')?>"><i class="fa fa-dashboard fa-fw"></i>Register New</a>
                    </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Work Shedule<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url('/work/add') ?>">Add Work</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('/work/update') ?>">Update Work</a>
                                </li>
                            </ul>
                            <!-- second-level-items -->
                        </li>
                        <li>
                            <a href="<?php echo site_url('person/leavelist/') ?>"><i class="fa fa-flask fa-fw"></i>Leave Request</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i>OVERTIME<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('/index.php/overtime/'); ?>">ADD OVERTIME</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('/index.php/overtime/publicHolidayAndOffduty'); ?>">P/HOLIDAY AND OFF DUTY</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('/index.php/overtime/OvertimeSummary'); ?>">OVERTIME SUMMARY</a>
                                </li>
                            </ul>
                            <!-- second-level-items -->
                        </li>
                        <li>
                            <a href="<?php echo site_url('person/index/') ?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a>

                        </li>
                        <li>

                        </li>
                        <li>
                            <!-- end side-menu -->
                            </div>

                            </nav>
                            <div id="page-wrapper">
                                <div class="row">
                                    <!-- Page Header -->
                                    <div class="col-lg-12">
                                        <h1 class="page-header">

                                            <script language="JavaScript1.2">


                                                var message = "Nurses Information Management System"
                                                var neonbasecolor = "#d1d4d7"
                                                var neontextcolor = "#04B173"
                                                var flashspeed = 100

                                                var n = 0
                                                if (document.all || document.getElementById) {
                                                    document.write('')
                                                    for (m = 0; m < message.length; m++)
                                                        document.write('<span id="neonlight' + m + '">' + message.charAt(m) + '</span>')
                                                    document.write('')
                                                }
                                                else
                                                    document.write(message)

                                                function crossref(number) {
                                                    var crossobj = document.all ? eval("document.all.neonlight" + number) : document.getElementById("neonlight" + number)
                                                    return crossobj
                                                }

                                                function neon() {

                                                    //Change all letters to base color
                                                    if (n == 0) {
                                                        for (m = 0; m < message.length; m++)
                                                            //eval("document.all.neonlight"+m).style.color=neonbasecolor
                                                            crossref(m).style.color = neonbasecolor
                                                    }

                                                    //cycle through and change individual letters to neon color
                                                    crossref(n).style.color = neontextcolor

                                                    if (n < message.length - 1)
                                                        n++
                                                    else {
                                                        n = 0
                                                        clearInterval(flashing)
                                                        setTimeout("beginneon()", 1500)
                                                        return
                                                    }
                                                }

                                                function beginneon() {
                                                    if (document.all || document.getElementById)
                                                        flashing = setInterval("neon()", flashspeed)
                                                }
                                                beginneon()


                                            </script>

                                        </h1>
                                    </div><!--New Content-->
                                    <!--End Page Header -->
                                </div>
                                <!--start of pubudu added code -->

                                <div class="content">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="formGroupInputSmall">Ward</label>
                                        <div class="col-xs-4">   
                                            <select class="form-control" id="ward" name="ward" onchange="getNursesByWard()" onclick="clearovertimeAll()">
                                            </select>
                                        </div>    
                                    </div>
                                    <br></br>
                                    <br></br>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="formGroupInputSmall">Name</label>
                                        <div class="col-xs-4">      
                                            <select class="form-control " id="name" name="name" onchange="clearovertimeAll()">
                                            </select>
                                        </div>    
                                    </div> 
                                    <br></br>
                                    <div class="form-group">
                                        <label for="formGroupInputSmall" class="col-sm-4 control-label">Overtime Commence</label>
                                        <div class="input-group date form_datetime col-xs-4" data-date="2014-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                            <input class="form-control" size="16" type="text" value="" id="starttime" onchange="showtime()">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/bootstrap/js/bootstrap.min.js"></script>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/js/locales/bootstrap-datetimepicker.uk.js" charset="UTF-8"></script>

                                            <script type="text/javascript">

                                                //in this line of code, to display the datetimepicker,  we used ‘form_datetime’ as an argument to be 
                                                //passed in javascript. This is for Date and Time.
                                                $('.form_datetime').datetimepicker({
                                                    language: 'en',
                                                    weekStart: 1,
                                                    todayBtn: 1,
                                                    autoclose: 1,
                                                    todayHighlight: 1,
                                                    startView: 2,
                                                    forceParse: 0,
                                                    showMeridian: 1
                                                });
                                                //this is for Date only	
                                                $('.form_date').datetimepicker({
                                                    language: 'en',
                                                    weekStart: 1,
                                                    todayBtn: 1,
                                                    autoclose: 1,
                                                    todayHighlight: 1,
                                                    startView: 2,
                                                    minView: 2,
                                                    forceParse: 0
                                                });
                                                //this is for Time Only	
                                                $('.form_time').datetimepicker({
                                                    language: 'en',
                                                    weekStart: 1,
                                                    todayBtn: 1,
                                                    autoclose: 1,
                                                    todayHighlight: 1,
                                                    startView: 1,
                                                    minView: 0,
                                                    maxView: 1,
                                                    forceParse: 0
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <br></br>
                                    <div class="form-group">
                                        <label for="dtp_input1" class="col-sm-4 control-label">Overtime End</label>
                                        <div class="input-group date form_datetime col-xs-4" data-date="2014-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                            <input class="form-control" size="16" type="text" value="" id="endtime" onchange="showtime()">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/bootstrap/js/bootstrap.min.js"></script>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
                                            <script type="text/javascript" src="<?php echo base_url(); ?>datepicker/datepicker/js/locales/bootstrap-datetimepicker.uk.js" charset="UTF-8"></script>

                                            <script type="text/javascript">

                                                //in this line of code, to display the datetimepicker,  we used ‘form_datetime’ as an argument to be 
                                                //passed in javascript. This is for Date and Time.
                                                $('.form_datetime').datetimepicker({
                                                    language: 'en',
                                                    weekStart: 1,
                                                    todayBtn: 1,
                                                    autoclose: 1,
                                                    todayHighlight: 1,
                                                    startView: 2,
                                                    forceParse: 0,
                                                    showMeridian: 1
                                                });
                                                //this is for Date only	
                                                $('.form_date').datetimepicker({
                                                    language: 'en',
                                                    weekStart: 1,
                                                    todayBtn: 1,
                                                    autoclose: 1,
                                                    todayHighlight: 1,
                                                    startView: 2,
                                                    minView: 2,
                                                    forceParse: 0
                                                });
                                                //this is for Time Only	
                                                $('.form_time').datetimepicker({
                                                    language: 'en',
                                                    weekStart: 1,
                                                    todayBtn: 1,
                                                    autoclose: 1,
                                                    todayHighlight: 1,
                                                    startView: 1,
                                                    minView: 0,
                                                    maxView: 1,
                                                    forceParse: 0
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <br></br>
                                    <button class="btn btn-primary" id="btnSearch" onclick="getAlert()">Submit Overtime</button>
                                    <br></br>


                                </div>

                                <!-- end of pubudu added code-->
                            </div>
                            <!--End of New content-->
                            </div>	

                            <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-1.10.2.js"></script>
                            <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap/bootstrap.min.js"></script>
                            <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/metisMenu/jquery.metisMenu.js"></script>
                            <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/pace/pace.js"></script>
                            <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/siminta.js"></script>



                            </body>

                            </html>
